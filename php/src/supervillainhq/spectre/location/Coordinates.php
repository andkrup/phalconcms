<?php
namespace supervillainhq\spectre\location{
	use supervillainhq\spectre\db\DataReader;

	class Coordinates{
		protected $latitude;
		protected $longitude;

		function latitude($latitude = null){
			if(is_null($latitude)){
				return $this->latitude;
			}
			$this->latitude = $latitude;
		}
		function longitude($longitude = null){
			if(is_null($longitude)){
				return $this->longitude;
			}
			$this->longitude = $longitude;
		}

		function __toString(){
			return "{$this->latitude()}, {$this->longitude()}";
		}

		function toGoogleLatLngLiteral(){
			return '{'."lat:{$this->latitude()}, lng:{$this->longitude()}".'}';
		}

		static function inflate(DataReader $dataReader){
			$instance = new Coordinates();
			$instance->latitude = $dataReader->coordinate_latitude;
			$instance->longitude = $dataReader->coordinate_longitude;
			return $instance;
		}

		static function avg(array $coordinates){
		    $latitude = 0;
		    $longitude = 0;
		    $c = 0;
		    foreach ($coordinates as $coordinate){
		        $latitude += $coordinate->latitude();
		        $longitude += $coordinate->longitude();
		        $c++;
		    }
		    $avg = new Coordinates();
		    $avg->latitude($latitude / $c);
		    $avg->longitude($longitude / $c);
		    return $avg;
		}
	}
}