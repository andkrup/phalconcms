<?php
namespace supervillainhq\spectre{
	use supervillainhq\core\Encoder;
	use Phalcon\DiInterface;
	use supervillainhq\core\date\Date;

	class ObjectEncoder implements Encoder{
		protected $di;
		protected $value;

		function __construct(DiInterface $di, $value){
			$this->di = $di;
			$this->value = $value;
		}

		function encode(){
			if(is_scalar($this->value)){
				return $this->value;
			}
			elseif(is_string($this->value)){
				return $this->value;
			}
			elseif(is_array($this->value)){
				$array = [];
				foreach ($this->value as $key => $value){
					$encoder = new ObjectEncoder($this->di, $value);
					$array[$key] = $encoder->encode();
				}
				return $array;
			}
			elseif($this->value instanceof \DateTime){
				return $this->value->format(\DateTime::W3C);
			}
			else{
				$encoder = $this->di->getEncoderfactory([$this->value]);
				if(isset($encoder)){
					return $encoder->encode();
				}
			}
			return null;
		}
	}
}