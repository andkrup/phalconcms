<?php
namespace supervillainhq\spectre\community{
	use supervillainhq\core\locale\Localizing;

	trait Commenting{
		use Localizing;

		protected $locked;
		protected $comments;



		function commentsLocked(){
			return true == $this->locked;
		}
		function hasComments(){
			return count($this->comments) > 0;
		}

		function resetComments(array $comments = []){
			if($this->commentsLocked()){
				return;
			}
			$this->comments = $comments;
		}
		function addComment(Comment $comment){
			if($this->commentsLocked()){
				return;
			}
			array_push($this->comments, $comment);
		}
		function removeComment(Comment $comment){
			if($this->commentsLocked()){
				return;
			}
			$c = count($this->comments);
			for($i = 0; $i < $c; $i++){
				if($this->comments[$i] == $comment){
					array_splice($this->comments, $i, 0);
				}
			}
		}
		function hasComment(Comment $comment){
			return in_array($this->comments, $comment);
		}
		function getComment($index){
			return $this->comments[$index];
		}
		function comments(){
			return $this->comments;
		}
	}
}