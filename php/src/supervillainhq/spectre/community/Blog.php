<?php
namespace supervillainhq\spectre\community{
	use Phalcon\DiInterface;
	use Phalcon\Config;
	use Phalcon\Di\InjectionAwareInterface;
	use supervillainhq\spectre\DependencyInjecting;

	class Blog implements InjectionAwareInterface{
		use DependencyInjecting;

		private $config;

		function __construct(DiInterface $dependencyInjector, Config $config){
			$this->setDI($dependencyInjector);
			$this->config = $config;
		}

		function posts(){
			$mapper = $this->di->getObjectmapper('post');
			return $mapper->all();
		}
	}
}