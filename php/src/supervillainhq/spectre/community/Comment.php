<?php
namespace supervillainhq\spectre\community{
	use supervillainhq\core\locale\Localized;
	use supervillainhq\spectre\posting\Author;

	interface Comment extends Localized{
		function author(Author $author = null);
		function created(\DateTime $date = null);
		function updated(\DateTime $date = null);
		function content();
	}
}