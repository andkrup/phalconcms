<?php
namespace supervillainhq\spectre\community{
	interface Commentable{
		function commentsLocked();
		function hasComments();

		function resetComments(array $comments = []);
		function addComment(Comment $comment);
		function removeComment(Comment $comment);
		function hasComment(Comment $comment);
		function getComment($index);
		function comments();
	}
}