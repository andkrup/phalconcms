<?php
namespace supervillainhq\spectre\posting\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\posting\NewsPost;

	class NewsPostMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof NewsPost){
					$this->addParameter('id', $data->id());
					$this->addParameter('title', $data->title());
					$this->addParameter('slug', $data->slug());
					$this->addParameter('summary', $data->summary());
					$this->addParameter('content', $data->content());
					$this->addParameter('created', $data->created() ? $data->created()->format('Y-m-d H:i:s') : null);
					$this->addParameter('updated', $data->updated() ? $data->updated()->format('Y-m-d H:i:s') : null);
					$this->addParameter('publish_date', $data->publish_date() ? $data->publish_date()->format('Y-m-d H:i:s') : null);
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('post_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$sql = "select
						p.id as post_id,
						p.title as post_title,
						p.slug as post_slug,
						p.summary_html as post_summary,
						p.content_html as post_content,
						p.publish_date as post_publish_date,
						p.created as post_created,
						p.updated as post_updated
					from blog_Posts p
					where p.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			$row = $query->fetch();
			if(isset($row)){
				$mapper = $this->getDI()->getObjectmapper('post', (array) $row);
				return $mapper->inflate();
			}
			return null;
		}

		function get(){
			return $this->find();
		}

		function exists(){}

		function all(){
			$sql = "select
						p.id as post_id,
						p.title as post_title,
						p.slug as post_slug,
						p.summary_html as post_summary,
						p.content_html as post_content,
						p.publish_date as post_publish_date,
						p.created as post_created,
						p.updated as post_updated
					from blog_Posts p
					order by publish_date desc;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$posts = [];
			foreach ($rows as $row){
				$mapper = $this->getDI()->getObjectmapper('post', (array) $row);
				$post = $mapper->inflate();
				array_push($posts, $post);
			}
			return $posts;
		}

		function reset(array $data = null){
			$this->data = $data;
		}

		function inflate(){
			return NewsPost::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'title':
					case 'slug':
					case 'summary':
					case 'summary_raw':
					case 'summary_html':
					case 'content':
					case 'content_raw':
					case 'content_html':
						return stripslashes(trim($this->getParameter($name)));
					case 'publish_date':
					case 'created':
					case 'updated':
						$value = $this->getParameter($name);
						return $value ? \DateTime::createFromFormat('Y-m-d H:i:s', $value) : null;
				}
			}
			return null;
		}
	}
}