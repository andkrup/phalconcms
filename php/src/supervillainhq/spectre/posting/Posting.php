<?php
namespace supervillainhq\spectre\posting{
	use supervillainhq\core\locale\Localizing;

	trait Posting{
		use Localizing;

		protected $title;
		protected $slug;
		protected $author;
		protected $created;

		function title($title = null){
			if(!is_null($title)){
				$this->title = $title;
			}
			return $this->title;
		}
		function slug($slug = null){
			if(!is_null($slug)){
				$this->slug = $slug;
			}
			return $this->slug;
		}
		function author(Author $author = null){
			if(!is_null($author)){
				$this->author = $author;
			}
			return $this->author;
		}
		function created(\DateTime $date = null){
			if(!is_null($date)){
				$this->created = $date;
			}
			return $this->created;
		}
		function uri(){
			return $this->slug();
		}
	}
}