<?php
namespace supervillainhq\spectre\posting{
	use supervillainhq\spectre\community\Commentable;
	use supervillainhq\spectre\community\Commenting;
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\cms\Editor;

	class NewsPost implements Post, Commentable, \JsonSerializable{
		use DataAware, Commenting, Posting{
			Posting::locale insteadof Commenting;
			Posting::language insteadof Commenting;
			Posting::region insteadof Commenting;
		}

		protected $publishDate;
		protected $updatedDate;
		protected $editor;
		protected $summary;
		protected $content;


		function published(\DateTime $datetime = null){
			if(is_null($datetime)){
				return $this->publishDate;
			}
			$this->publishDate = $datetime;
		}
		function updated(\DateTime $datetime = null){
			if(is_null($datetime)){
				return $this->updatedDate;
			}
			$this->updatedDate = $datetime;
		}
		function summary($summary = null){
			if(is_null($summary)){
				return $this->summary;
			}
			$this->summary = $summary;
		}
		function content($content = null){
			if(is_null($content)){
				return $this->content;
			}
			$this->content = $content;
		}
		function editor(Editor $editor = null){
			if(is_null($editor)){
				return $this->editor;
			}
			$this->editor = $editor;
		}

		function uri(){
			return $this->slug();
		}

		static function inflate(DataReader $reader){
			$instance = new NewsPost();
// 			$instance->author = $reader->author;
			$instance->content = $reader->content;
			$instance->summary = $reader->summary;
			$instance->title = $reader->title;
			$instance->slug = $reader->slug;
			return $instance;
		}

		function jsonSerialize(){
			$published = $this->published();
			$visibles = [
					'title' => $this->title(),
					'summary' => $this->summary(),
					'content' => $this->content(),
					'uri' => $this->uri(),
					'published' => $published ? $published->format(\DateTime::ATOM) : null
			];
			return (object) $visibles;
		}
	}
}