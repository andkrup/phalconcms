<?php
namespace supervillainhq\spectre\posting{
	interface Author{
		function name();
		function email();
	}
}