<?php
namespace supervillainhq\spectre\posting{
	use supervillainhq\core\locale\Localized;

	interface Post extends Localized{
		function title($title = null);
		function slug($slug = null);
		function uri();
		function author(Author $author = null);
		function created(\DateTime $date = null);
	}
}