<?php
namespace supervillainhq\spectre\contacts\db{
	use supervillainhq\core\contacts\PostalCode;
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\contacts\Address;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\intl\db\CountryMapper;

	class AddressMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof Address){
					$this->addParameter('id', $data->id());
	    			$this->addParameter('fullname', $data->fullname());
	    			$this->addParameter('attention', $data->att());
					$this->addParameter('address1', $data->address1());
					$this->addParameter('address2', $data->address2());
					$this->addParameter('city', $data->city());
					$this->addParameter('country', $data->country());
					$this->addParameter('fullname', $data->fullname());
					$this->addParameter('postalcode', $data->postalcode());
					$this->addParameter('region', $data->region());
					$this->addParameter('att', $data->att());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('address_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$searchables = ['id'];
//			$results = [];
			while (count($searchables) > 0) {
				$search = array_shift($searchables);
				if ($this->hasParameterAtKey($search)) {
					$value = $this->getParameter($search);
					$sql = "select
								a.id as address_id,
								a.fullname as address_fullname,
								a.attention as address_attention,
								a.address1 as address_address1,
								a.address2 as address_address2,
								a.postalcode as address_postalcode,
								a.city as address_city,
								a.county as address_county,
								a.region as address_region
								, c.id as country_id, c.iso as country_iso, c.iso3 as country_iso3, c.nicename as country_name, c.local_name as country_local_name, c.phonecode as country_callcode
							from cms_Addresses a
							left join Countries c on c.id = a.country_id
							where a.id = :id;";
					$query = SqlQuery::create($sql);
					$query->query([$search => $value]);
					$row = $query->fetch();
					if ($row) {
						$addressMapper = new AddressMapper((array)$row);
						if ($address = $addressMapper->inflate()) {
							$countryMapper = new CountryMapper((array)$row);
							if ($country = $countryMapper->inflate()) {
								$address->country($country);
							}
						}

						return $address;
					}
				}
			}
			return null;
		}

		function get(){
			return $this->find();
		}

		function exists(){
			$sql = "select 1 as existing
					from cms_Addresses a
					where s.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			return $query->fetchValue('existing', SqlQuery::TYPE_BOOLEAN);
		}

		function all(){
			$sql = "select
						a.id as address_id,
						a.fullname as address_fullname,
						a.attention as address_attention,
						a.address1 as address_address1,
						a.address2 as address_address2,
						a.postalcode as address_postalcode,
						a.city as address_city,
						a.county as address_county,
						a.region as address_region
						, c.id as country_id, c.iso as country_iso, c.iso3 as country_iso3, c.nicename as country_name, c.local_name as country_local_name, c.phonecode as country_callcode
					from cms_Addresses a
					left join Countries c on c.id = a.country_id;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$addresses = [];
			foreach ($rows as $row){
				$addressMapper = new AddressMapper((array) $row);
				if($address = $addressMapper->inflate()){
					$countryMapper = new CountryMapper((array) $row);
					if($country = $countryMapper->inflate()){
						$address->country($country);
					}
					array_push($addresses, $address);
				}
			}
			return $addresses;
		}

		function reset(array $data = null){
			$this->resetParameters($data);
		}

		function inflate(){
			return Address::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'fullname':
					case 'attention':
					case 'address1':
					case 'address2':
					case 'city':
					case 'county':
					case 'region':
						return stripslashes(trim($this->getParameter($name)));
					case 'postalcode':
						return new PostalCode(stripslashes(trim($this->getParameter($name))));
				}
			}
			return null;
		}
	}
}
