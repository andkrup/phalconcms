<?php
namespace supervillainhq\spectre\contacts\db{
	use supervillainhq\core\contacts\PostalCode;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\contacts\Address;

	class AddressWriter implements DataWriter{
		use ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof Address){
					$this->addParameter('id', $data->id());
	    			$this->addParameter('fullname', $data->fullname());
	    			$this->addParameter('attention', $data->att());
	    			$this->addParameter('address1', $data->address1());
	    			$this->addParameter('address2', $data->address2());
	    			$this->addParameter('city', $data->city());
	    			$this->addParameter('postalcode', $data->postalcode()->__toString());
	    			$this->addParameter('country', $data->country()->name());
	    			$this->addParameter('region', $data->region());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('address_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into cms_Addresses
					(fullname, attention, address1, address2, postalcode, city, country, region)
					values (:fullname, :attention, :address1, :address2, :postalcode, :city, :country, :region);";
			$parameters = [
					'fullname' => $this->fullname,
					'attention' => $this->attention,
					'address1' => $this->address1,
					'address2' => $this->address2,
					'postalcode' => $this->postalcode,
					'city' => $this->city,
					'country' => $this->country,
					'region' => $this->region,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}
		function update(){
			$sql = "update cms_Addresses set
					fullname = :fullname,
					attention = :attention,
					address1 = :address1,
					address2 = :address2,
					postalcode = :postalcode,
					city = :city,
					country = :country,
					region = :region
					where id = :id;";
			$parameters = [
					'fullname' => $this->fullname,
					'attention' => $this->attention,
					'address1' => $this->address1,
					'address2' => $this->address2,
					'postalcode' => $this->postalcode,
					'city' => $this->city,
					'country' => $this->country,
					'region' => $this->region,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}
		function delete(){}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'fullname':
					case 'attention':
					case 'address1':
	    			case 'address2':
	    			case 'city':
					case 'country':
					case 'region':
						return stripslashes(trim($this->getParameter($name)));
					case 'postalcode':
						return new PostalCode(stripslashes(trim($this->getParameter($name))));
				}
			}
			return null;
		}
	}
}