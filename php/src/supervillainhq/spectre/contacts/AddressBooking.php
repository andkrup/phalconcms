<?php
namespace supervillainhq\spectre\contacts{
	trait AddressBooking{
		protected $addresses;

		function resetAddresses(array $addresses = []){
			$this->addresses = $addresses;
		}
		function addAddress($key, Address $address){
			$this->addresses[$key] = $address;
		}
		function removeAddress(Address $address){
			$c = count($this->addresses);
			for($i = 0; $i < $c; $i++){
				if($this->addresses[$i] == $address){
					array_splice($this->addresses, $i, 0);
				}
			}
		}
		function removeAddressAt($key){
			unset($this->addresses[$key]);
		}
		function hasAddress(Address $address){
			return in_array($this->addresses, $address);
		}
		function hasAddressAtKey($key){
			return array_key_exists($key, $this->addresses);
		}
		function getAddress($key){
			return $this->addresses[$key];
		}
		function addresses(){
			return $this->addresses;
		}
	}
}