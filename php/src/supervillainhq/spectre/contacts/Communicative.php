<?php
namespace supervillainhq\spectre\contacts{

	use supervillainhq\core\contacts\PhoneNumber;
	use supervillainhq\core\mail\Email;

	trait Communicative{
		private $emails;
		private $phones;

		function resetEmails(array $emails = []){
			$this->emails = $emails;
		}
		function addEmail($key, Email $email){
			if(!is_array($this->emails)){
				$this->resetEmails();
			}
			$this->emails[$key] = $email;
		}
		function removeEmail(Email $email){
			$c = count($this->emails);
			for($i = 0; $i < $c; $i++){
				if($this->emails[$i] == $email){
					array_splice($this->emails, $i, 0);
				}
			}
		}
		function removeEmailAt($key){
			unset($this->emails[$key]);
		}
		function hasEmail(Email $email){
			return in_array($this->emails, $email);
		}
		function hasEmailAtKey($key){
			if(!is_array($this->emails)){
				$this->resetEmails();
				return false;
			}
			return array_key_exists($key, $this->emails);
		}
		function getEmail($key){
			return $this->emails[$key];
		}
		function emails(){
			return $this->emails;
		}

		function resetPhones(array $phones = []){
			$this->phones = $phones;
		}
		function addPhone($key, PhoneNumber $phone){
			$this->phones[$key] = $phone;
		}
		function removePhone(PhoneNumber $phone){
			$c = count($this->phones);
			for($i = 0; $i < $c; $i++){
				if($this->phones[$i] == $phone){
					array_splice($this->phones, $i, 0);
				}
			}
		}
		function removePhoneAt($key){
			unset($this->phones[$key]);
		}
		function hasPhone(PhoneNumber $phone){
			return in_array($this->phones, $phone);
		}
		function hasPhoneAtKey($key){
			return array_key_exists($key, $this->phones);
		}
		function getPhone($key){
			return $this->phones[$key];
		}
		function phones(){
			return $this->phones;
		}
	}
}