<?php
namespace supervillainhq\spectre\contacts{
	interface Contact{
		function info(ContactInfo $info = null);
	}
}