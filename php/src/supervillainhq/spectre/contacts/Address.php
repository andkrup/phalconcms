<?php
namespace supervillainhq\spectre\contacts{

	use supervillainhq\core\Objectible;
	use supervillainhq\core\db\DataAware;
	use supervillainhq\core\contacts\PostalCode;
	use supervillainhq\core\intl\Country;
	use supervillainhq\spectre\db\DataReader;

	class Address implements \Serializable{
		use DataAware;
		use Objectible;

		private $fullname;
		private $att;
		private $address1;
		private $address2;
		private $city;
		private $postalcode;
		private $region;
		private $country;

		function fullname( $fullname = null){
			if(is_null($fullname)){
				return $this->fullname;
			}
			$this->fullname = $fullname;
		}
		function att( $attention = null){
			if(is_null($attention)){
				return $this->att;
			}
			$this->att = $attention;
		}
		function address1( $address = null){
			if(is_null($address)){
				return $this->address1;
			}
			$this->address1 = $address;
		}
		function address2( $address = null){
			if(is_null($address)){
				return $this->address2;
			}
			$this->address2 = $address;
		}
		function city( $city = null){
			if(is_null($city)){
				return $this->city;
			}
			$this->city = $city;
		}
		function postalcode(PostalCode $code = null){
			if(is_null($code)){
				return $this->postalcode;
			}
			$this->postalcode = $code;
		}
		function region( $region = null){
			if(is_null($region)){
				return $this->region;
			}
			$this->region = $region;
		}
		function country(Country $country = null){
			if(is_null($country)){
				return $this->country;
			}
			$this->country = $country;
		}

		/**
		 * Returns true if the id matches.
		 *
		 * @param Address $address
		 * @return boolean
		 */
		function is(Address $address){
			return $address->id() === $this->id();
		}
		/**
		 * Returns true if the selected parameters matches. By default this function compares the address1, postalcode,
		 * city, region and country values and returns true if all parameters are equal. Using the additional optional
		 * parameters will include these to the matching.
		 *
		 * @param Address $address
		 * @param string $matchFullnames (optional) If a truthey value is inserted, then this function will also try to match the fullname parameter
		 * @param string $matchAddress2 (optional) If a truthey value is inserted, then this function will also try to match the address2 parameter
		 * @param string $matchAttention (optional) If a truthey value is inserted, then this function will also try to match the attention parameter
		 */
		function equals(Address $address, $matchFullnames = false, $matchAddress2 = false, $matchAttention = false){
			$fullnameMatch = strtolower($this->fullname) == strtolower($address->fullname);
			$attentionMatch = strtolower($this->att) == strtolower($address->att);
			$address1Match = strtolower($this->address1) == strtolower($address->address1);
			$address2Match = strtolower($this->address2) == strtolower($address->address2);
			$zipMatch = strtolower($this->postalcode) == strtolower($address->postalcode);
			$cityMatch = strtolower($this->city) == strtolower($address->city);
			$countryMatch = strtolower($this->country) == strtolower($address->country);
			$regionMatch = strtolower($this->region) == strtolower($address->region);
			if($address1Match && $zipMatch && $cityMatch && $countryMatch && $regionMatch){
				if($matchFullnames && !$fullnameMatch){
					return false;
				}
				if($matchAddress2 && !$address2Match){
					return false;
				}
				if($matchAttention && !$attentionMatch){
					return false;
				}
				return true;
			}
			return false;
		}

		function blank(){
			return strlen("{$this->fullname}{$this->att}{$this->address1}{$this->address2}{$this->city}{$this->postalcode}{$this->country}") === 0;
		}


		function serialize(){
			$object = $this->toObject();
			return serialize($object);
		}
		function unserialize($serialized){
			if(is_null($serialized)){
				return;
			}
			elseif(!is_object($serialized)){
				$serialized = unserialize($serialized);
			}
			$data = (object) $serialized;
			$this->id = isset($data->id) ? intval($data->id) : null;
			$this->fullname = isset($data->fullname) ? $data->fullname : null;
			$this->att = isset($data->att) ? $data->att : null;
			$this->address1 = isset($data->address1) ? $data->address1 : null;
			$this->address2 = isset($data->address2) ? $data->address2 : null;
			$this->city = isset($data->city) ? $data->city : null;
			$this->postalcode = isset($data->postalcode) ? new PostalCode($data->postalcode) : null;
			$this->country = isset($data->country) ? Country::get($data->country) : null;
			$this->region($data->region);
		}

		static function hydrate($serialized){
			$instance = new Address();
			$instance->unserialize($serialized);
			return $instance;
		}
		static function inflate(DataReader $reader){
			$instance = new Address();
			$instance->id($reader->id);
			$instance->fullname($reader->fullname);
			$instance->att($reader->att);
			$instance->address1($reader->address1);
			$instance->address2($reader->address2);
			$instance->city($reader->city);
			$instance->postalcode(new PostalCode($reader->postalcode));
//			$instance->country(Country::get($reader->country));
			$instance->region($reader->region);
			return $instance;
		}
	}
}