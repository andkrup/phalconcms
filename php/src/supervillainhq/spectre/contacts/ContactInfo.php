<?php
namespace supervillainhq\spectre\contacts{

	class ContactInfo implements \Serializable{
		use Communicative;


		function __construct(array $emails = null, array $phones = null){
			if(is_null($emails)){
				$emails = [];
			}
			if(is_null($phones)){
				$phones = [];
			}
			$this->resetEmails($emails);
			$this->resetPhones($phones);
		}


		function serialize(){
			$object = (object) [
					'emails' => $this->emails,
					'phones' => $this->phones,
			];
			return serialize($object);
		}

		function unserialize ($serialized) {
			// handle the situations where an already unserialized object is passed by recursed unserialize calls
			if(is_null($serialized)){
				return;
			}
			elseif(is_object($serialized)){
				$this->resetEmails($serialized->emails);
				$this->resetPhones($serialized->phones);
			}
			else{
				// $serialized is a string-interpretation and should be unserialized
				$data = unserialize($serialized); // $serialized is now a stdClass/simple object
				$this->resetEmails($data->emails);
				$this->resetPhones($data->phones);
			}
		}

	}
}