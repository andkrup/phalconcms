<?php
namespace supervillainhq\spectre{
	use Phalcon\Di\InjectionAwareInterface;
	use Phalcon\DiInterface;

	class DataObjectFactory implements InjectionAwareInterface{
		use DependencyInjecting;

		private $map;

		function __construct(array $map, DiInterface $dependencyInjector = null){
			$this->map = (object) $map;
			$this->setDI($dependencyInjector);
		}

		function addMappers(array $mappers){
			$this->map->mappers = array_merge($this->map->mappers, $mappers);
		}

		function getMapper($type, array $parameters = null){
			$class = $this->map->mappers[$type];
			if(isset($class)){
				$reflector = new \ReflectionClass($class);
				if(empty($parameters)){
					$instance = $reflector->newInstance();
				}
				else{
					$instance = $reflector->newInstanceArgs($parameters);
				}
				if(in_array('supervillainhq\spectre\db\DataMapper', class_implements($instance))){
					$instance->setDI($this->getDI());
					return $instance;
				}
			}
			return null;
		}

		function addWriters(array $writers){
			$this->map->writers = array_merge($this->map->writers, $writers);
		}

		function getWriter($type, array $parameters = null){
			$class = $this->map->writers[$type];
			if(isset($class)){
				$reflector = new \ReflectionClass($class);
				if(empty($parameters)){
					$instance = $reflector->newInstance();
				}
				else{
					$instance = $reflector->newInstanceArgs($parameters);
				}
				if(in_array('supervillainhq\core\db\DataWriter', class_implements($instance))){
					$instance->setDI($this->getDI());
					return $instance;
				}
			}
			return null;
		}
	}
}