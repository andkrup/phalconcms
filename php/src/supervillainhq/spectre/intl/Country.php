<?php
namespace supervillainhq\spectre\intl{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\core\intl\Country as CoreCountry;
	use supervillainhq\spectre\db\DataReader;

	class Country extends CoreCountry implements \Serializable{
		use DataAware;

		private $callCode;


		/**
		 * Phone number prefix
		 *
		 * @param string $callCode
		 * @return string
		 */
		function callCode( $callCode = null){
			if(is_null($callCode)){
				return $this->callCode;
			}
			$this->callCode = $callCode;
		}




//		static function get($countryName){
//			$instance = new Country();
//			$instance->name($countryName);
//			return $instance;
//		}

		static function inflate(DataReader $reader){
			$instance = new Country();
			$instance->id = $reader->id;
			$instance->callCode = $reader->callcode;
			$instance->isoCodeAlpha2 = $reader->iso;
			$instance->isoCodeAlpha3 = $reader->iso3;
			$instance->name = $reader->name;
			return $instance;
		}



		function serialize(){
			$object = (object) ['name'=>$this->name];
			return serialize($object);
		}
		function unserialize($serialized){}


		function __toString(){
			return "{$this->name}";
		}
	}
}