<?php
namespace supervillainhq\spectre\intl\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\DependencyInjecting;
	use supervillainhq\spectre\cms\users\User;

	class CountryWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof User){
					$this->addParameter('id', $data->id());
					$this->addParameter('username', $data->name());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('user_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into Countries
					(nicename, local_name, iso, iso3)
					values (:name, :localname, :iso, :iso3);";
			$parameters = [
					'name' => $this->name,
					'localname' => $this->localname,
					'iso' => $this->iso,
					'iso3' => $this->iso3
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return intval($query->lastInsertId());
		}

		function update(){
			$sql = "update Countries set
					nicename = :name,
					localname = :localname,
					iso = :iso,
					iso3 = :iso3
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'name' => $this->name,
					'localname' => $this->localname,
					'iso' => $this->iso,
					'iso3' => $this->iso3
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}

		function delete(){
			$sql = "delete from Countries
					where id = :id;";
			$query = SqlQuery::create($sql);
			return $query->execute(['id' => $this->id]);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
					case 'callcode':
						return intval($this->getParameter($name));
					case 'name':
					case 'localname':
					case 'iso':
					case 'iso3':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}