<?php
namespace supervillainhq\spectre\intl\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\intl\Country;

	class CountryMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof Country){
					$this->addParameter('id', $data->id());
					$this->addParameter('iso', $data->isoCodeAlpha2());
					$this->addParameter('iso3', $data->isoCodeAlpha3());
					$this->addParameter('name', $data->name());
					$this->addParameter('localname', $data->localizedName());
					$this->addParameter('callcode', $data->callCode());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('country_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$sql = "select
						c.id as country_id, c.iso as country_iso, c.iso3 as country_iso3, c.nicename as country_name, c.local_name as country_local_name, c.phonecode as country_callcode
					from Countries c
					where c.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			$row = $query->fetch();
			if ($row){
				$mapper = new CountryMapper((array) $row);
				return $mapper->inflate();
			}
			return null;
		}

		function get(){
			return $this->find();
		}

		function exists(){
			$sql = "select 1 as existing
					from Countries c
					where c.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			return $query->fetchValue('existing', SqlQuery::TYPE_BOOLEAN);
		}

		function all(){
			$sql = "select
						c.id as country_id, c.iso as country_iso, c.iso3 as country_iso3, c.nicename as country_name, c.local_name as country_local_name, c.phonecode as country_callcode
					from Countries c;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$countries = [];
			foreach ($rows as $row){
				$countryMapper = new CountryMapper((array) $row);
				if($country = $countryMapper->inflate()){
					array_push($countries, $country);
				}
			}
			return $countries;
		}

		function reset(array $data = null){
			$this->resetParameters($data);
		}

		function inflate(){
			return Country::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
					case 'callcode':
						return intval($this->getParameter($name));
					case 'name':
					case 'localname':
					case 'iso':
					case 'iso3':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}