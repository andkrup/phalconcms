<?php
namespace supervillainhq\spectre\db{
	use Phalcon\DiInterface;
	use Phalcon\Di\InjectionAwareInterface;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;

	class Mapper implements InjectionAwareInterface{
	    use ParameterContainer, Reading;

	    private $di;
	    protected $lazyLoad;


	    function __construct($data = null, $lazyLoad = true){
	    	$this->resetParameters();
	    	if(!is_null($data)){
	    		$this->data($data);
	    	}
	    	$this->lazyLoad = $lazyLoad;
	    }

		function lazyLoad($bool = true){
			$this->lazyLoad = $bool;
		}

		public function setDI(DiInterface $dependencyInjector){
			$this->di = $dependencyInjector;
		}

		public function getDI(){
			return $this->di;
		}
	}
}