<?php
namespace supervillainhq\spectre\db{
	interface IsReadable extends DataReader{
		function dataReader(DataReader $dataReader = null);
	}
}