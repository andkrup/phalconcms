<?php
namespace supervillainhq\arkham\db\connectors{
	/**
	 *
	 * @author Andkrup
	 *
	 */
	class SQLiteConnector implements DatabaseConnector{
		function __construct(){

		}

		public function viewQuery(){
			return null;
		}

		public function getPreviousQuery(){
			return "";
		}

		public function getErrorMessage(){
			return "";
		}

		public function getResult(){
			return "";
		}
		public function getInsertId(){
			return false;
		}
		public function getVerbose(){
			return false;
		}
		public function setVerbose($value){
		}

		public function fetchSingleValue($query, $params){
		}

		public function fetchResultSet($query, $params){
		}

		public function execute($query, $params){
		}
	}
}
?>