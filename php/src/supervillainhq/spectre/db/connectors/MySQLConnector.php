<?php
namespace supervillainhq\arkham\db\connectors{
	use Phalcon\Config;
	/**
	 *
	 * @author Andkrup
	 *
	 */

	class MySQLConnector implements DatabaseConnector{
		const DATETIME_FORMAT = 'Y-m-d H:i:s';
		protected $query;
		protected $host;
		protected $database;
		protected $user;
		protected $pass;
		protected $encoding;
		protected $result;
		private $insertId;
		private $verbose;
		private $dbconfig;
		private $mysqli;

		public function __construct(Config $config){
			$this->dbconfig = $config;
			$this->host = $this->dbconfig->getValue('host');
			$this->database = $this->dbconfig->getValue('database');
			$this->user = $this->dbconfig->getValue('user');
			$this->pass = $this->dbconfig->getValue('pass');
			$this->encoding = $this->dbconfig->getValue('encoding');
		}

		public function hasValidConfig(){
			return null!=$this->host && null!=$this->database && null!=$this->user && null!=$this->pass;
		}

		public function viewQuery(){
			return $this->query;
		}

		public function fetchSingleValue($query, $params){
		}

		public function fetchResultSet($query, $params){
			$this->mysqli = new \mysqli($this->host, $this->user, $this->pass, $this->database);
			if ($this->mysqli->connect_error) {
				throw new \Exception("Database connection error: {$this->mysqli->connect_error}", 301);
			}
			$this->mysqli->set_charset($this->encoding);
			$stmt = $this->mysqli->stmt_init();
			$arr = $this->rewriteAndExtractType($query);
			if ($stmt->prepare($arr->query)) {
				if(!is_null($arr->types) && count($params) > 0){
					array_unshift($params, $arr->types);
					$bindResult = call_user_func_array(array($stmt, 'bind_param'), $this->refValues($params));
				}
				$execResult = $stmt->execute();
				$variables = array();
				$data = array();
				$meta = $stmt->result_metadata();

				while($field = $meta->fetch_field()){
					$param[] = &$data[$field->name]; // pass by reference
				}

				$bindresultResult = call_user_func_array(array($stmt, 'bind_result'), $param);

				while ($stmt->fetch()) {
					foreach($data as $key => $val){
						$c[$key] = $val;
					}
					$result[] = (object) $c;
				}
				$stmt->close();
				$this->mysqli->close();
				if(isset($result)){
					$this->result = $result;
					$rowCount = count($result);
					return $rowCount;
				}
				return null;
			}
			else{
				throw new \Exception("failed to prepare statement ({$arr->query})");
			}
			if(count($this->mysqli->error_list ) > 0){
				throw new \Exception($this->mysqli->error);
			}
			$this->mysqli->close();
			return null;
		}

		public function execute($query, $params){
			$this->mysqli = new \mysqli($this->host, $this->user, $this->pass, $this->database);
			if ($this->mysqli->connect_error) {
				throw new \Exception("Database connection error: {$this->mysqli->connect_error}", 301);
			}
			$this->mysqli->set_charset($this->encoding);
			$stmt = $this->mysqli->stmt_init();
			unset($this->insertId);
			$result;
			$affectedRows = 0;
			$arr = $this->rewriteAndExtractType($query);
			if ($stmt->prepare($arr->query)) {
				if(!is_null($arr->types) && count($params) > 0){
					array_unshift($params, $arr->types);
					call_user_func_array(array($stmt, 'bind_param'), $this->refValues($params));
				}
				$result = $stmt->execute();
				if(stripos(trim($query), 'update') === 0 || stripos(trim($query), 'insert') === 0){
					$affectedRows = $this->mysqli->affected_rows;
				}
				$stmt->close();
				$this->insertId = $this->mysqli->insert_id;
			}
			else{
				throw new \Exception("failed to prepare statement ({$arr->query})");
			}
			if(count($this->mysqli->error_list ) > 0){
				throw new \Exception($this->mysqli->error);
			}
			$this->mysqli->close();
			if(!$result){
				throw new \Exception('the query compiled but failed');
			}
			if(stripos(trim($query), 'update') === 0 || stripos(trim($query), 'insert') === 0){
				return $affectedRows;
			}
			return -1;
		}
		public function getPreviousQuery(){
			return $this->query;
		}
		public function getErrorMessage(){
			return mysql_error($this->connection);
		}

		public function getResult(){
			return $this->result;
		}

		public function getInsertId(){
			return $this->insertId;
		}

		public function getVerbose(){
			return $this->verbose;
		}
		public function setVerbose($value){
			$this->verbose = $value;
		}

		private function refValues($arr){
			//Reference is required for PHP 5.3+
			if (strnatcmp(phpversion(),'5.3') >= 0){
				$refs = array();
				foreach($arr as $key => $value){
					$refs[$key] = &$arr[$key];
				}
				return $refs;
			}
			return $arr;
		}
		private function rewriteAndExtractType($query){
			$regexp = '/@\(([bdis]{1})[0-9]*\)/';
			$return = array();
			preg_match_all($regexp, $query, $matches);
			$return['query'] = preg_replace($regexp, '?', $query);
			if(isset($matches[1])){
				$return['types'] = implode('', $matches[1]);
			}
			return (object) $return;
		}

		private function connect(){
			if(!isset($this->connection)){
				$this->connection = mysql_connect($this->host, $this->user, $this->pass);
				if(isset($this->encoding)){
					$encRes = mysql_set_charset($this->encoding); // "utf8"
				}
			}
	//		echo "MySQLConnector:> connect() connection: ".(null!=$this->connection).", errs: ".mysql_error();
			return null!=mysql_error();
		}

		private function selectDb(){
	// 		echo "[MySQLConnector]:> ->selectDb()";
			if(!isset($this->connection)){
	// 			echo "[MySQLConnector]:> ->selectDb() no connection";
				return false;
			}
			return null!=mysql_select_db($this->database);
		}

		function __destruct(){
			if(isset($this->connection)){
				@mysql_close($this->connection);
				unset($this->connection);
	// 			$this->connection = null;
			}
		}

		public function __toString(){
			return "[MySQLConnector]";
		}
	}
}
?>