<?php
namespace supervillainhq\arkham\db\connectors{
	interface DatabaseConnector{
		public function fetchSingleValue($query, $params);
		public function fetchResultSet($query, $params);
		public function execute($query, $params);
		public function viewQuery();
		public function getErrorMessage();
		public function getPreviousQuery();
		public function getResult();
		public function getInsertId();
		public function getVerbose();
		public function setVerbose($value);
	}
}
?>