<?php
namespace supervillainhq\cheyenne\db\connectors{

	/**
	 * Enum of available dataconnectors. Comments describe how content is saved
	 */
	class DataConnectors{
		// content is in template files
		const NONE = "";
		// content is in text files, fetch lines in files instead of sql
		const TEXT = "text";
		// content is in a relational database that can be queried via SQL
		const SQLITE = "sqlite";
		const MYSQL = "mysql";
		const POSTGRES = "postgres";
		// content is in a non-sql database
		const MONGODB = "mongodb";
	}
}
?>