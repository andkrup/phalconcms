<?php
namespace supervillainhq\spectre\db{
	use Phalcon\Di\InjectionAwareInterface;

	/**
	 * The DataMapper is handling the task of mapping database information into domain-level objects.
	 * Use the DataWriter to handle tasks that goes the other way, ie. moving data from domain-level
	 * objects into the database.
	 *
	 * @author ak
	 *
	 */
	interface DataMapper extends InjectionAwareInterface{

		/**
		 * Standard crud below (actually it's only the R in cRud that is defined here)
		 */
		function find();
		function get();
		function exists();
		function all();

		/**
		 * Any concrete DataMappers should know how to handle the logic of transforming the datarow into
		 * a specific domain-level object. In other words: Implementations should know what class to
		 * instantiate, and how the data is mapped to properties.
		 */
		function inflate();
	}
}