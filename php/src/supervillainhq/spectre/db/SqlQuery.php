<?php
namespace supervillainhq\spectre\db{
// 	use Phalcon\Db\Adapter\Pdo;
	use Phalcon\Db\ResultInterface;

	/**
	 * Wrapper around the dbadapter that also adds query caching
	 *
	 * @author ak
	 *
	 */
	class SqlQuery{
		CONST TYPE_INT = 'integer';
		CONST TYPE_BOOLEAN = 'boolean';
		CONST TYPE_STRING = 'string';
		static $dbAdapter;
		static protected $cache;
		protected $rawStatement;
		protected $statement;
		protected $parameters;
		protected $dataTypes;
		protected $result;

		/**
		 *
		 * @param string $sql The statement to execute
		 * @param boolean $cache Signals whether to search for previously cached results and/or update the returned results
		 * @throws \Exception If the static DbAdapter reference is not set
		 * @return \supervillainhq\spectre\db\SqlQuery
		 */
		static function create($sql, $cache = false){
			if(!isset(self::$dbAdapter)){
				throw new \Exception('Static reference to DatabaseAdapter not set. Please assign a DbAdapter to SqlQuery::$dbAdapter');
			}
			return new SqlQuery($sql);
		}

		private function __construct($sql){
			$this->rawStatement = $sql;
		}

		/**
		 * For queries returning resultsets
		 *
		 * @param array $parameters
		 * @param array $dataTypes
		 */
		function query(array $parameters = null, array $dataTypes = null){
			$this->parameters = $parameters;
			$this->dataTypes = $dataTypes;

			self::$dbAdapter->connect();

			if(!is_null($this->parameters)){
				$this->statement = self::$dbAdapter->prepare($this->rawStatement);
				$this->result = self::$dbAdapter->executePrepared($this->statement, $this->parameters, $this->dataTypes);
			}
			else{
				$this->result = self::$dbAdapter->query($this->rawStatement);
			}
			if($this->result instanceof \PDOStatement){
				return 0 != $this->result->rowCount();
				$errorCode = $this->result->errorCode();
				$errorInfo = $this->result->errorInfo();
			}
			return false;
		}
		/**
		 * For queries not returning resultsets
		 *
		 * @param array $parameters
		 * @param array $dataTypes
		 */
		function execute(array $parameters = null, array $dataTypes = null){
			$this->parameters = $parameters;
			$this->dataTypes = $dataTypes;
			self::$dbAdapter->connect();
			if(!is_null($this->parameters)){
				$this->statement = self::$dbAdapter->prepare($this->rawStatement);
				$this->result = self::$dbAdapter->executePrepared($this->statement, $this->parameters, $this->dataTypes);
			}
			else{
				$this->result = self::$dbAdapter->query($this->rawStatement);
			}
			if($this->result instanceof \PDOStatement){
				return $this->result->errorCode() != '0000';
				$errorCode = $this->result->errorCode();
				$errorInfo = $this->result->errorInfo();
			}
			return false;
		}

		function rawStatement(){
			return $this->statement;
		}
		function rawResult(){
			return $this->result;
		}

		function fetchAll(){
			return $this->result->fetchAll();
			if($this->result instanceof ResultInterface){
			}
		}
		function fetchArray(){
			return $this->result->fetchArray();
			if($this->result instanceof ResultInterface){
			}
		}
		function fetch(array $attributes = null){
			if($this->result instanceof \PDOStatement){
				$row = $this->result->fetch();
			}
			$return = [];
			if(!$row){
				return $return;
			}
			if(is_array($attributes)){
				foreach ($attributes as $attribute){
					if(array_key_exists($attribute, $row)){
						$return[$attribute] = $row[$attribute];
					}
				}
				return (object) $return;
			}
			return (object) $row;
		}
		function fetchValue($value, $type = null){
			$row = $this->result->fetch();
			if(array_key_exists($value, $row)){
				switch(strtolower($type)){
					case 's':
					case self::TYPE_STRING:
						return (string) $row[$value];
					case 'i':
					case 'int':
					case self::TYPE_INT:
						return (integer) $row[$value];
					case 'b':
					case 'bool':
					case self::TYPE_BOOLEAN:
						return (boolean) $row[$value];
					default:
						return $row[$value];
				}
			}
		}

		function lastInsertId(){
			return self::$dbAdapter->lastInsertId();
		}
	}
}