<?php
namespace supervillainhq\spectre\db{
	use Phalcon\Di\InjectionAwareInterface;
	/**
	 * This interface is for helper objects that can load data into their
	 * domain objects.
	 * Most functions should depend on constructor injected data.
	 *
	 * @author ak
	 *
	 */
	interface DataReader extends InjectionAwareInterface{
		// below function have a default implementation in the trait Reading
		function data($data = null);
		// below functions have a default implementation in the trait ParameterContainer
		function resetParameters(array $parameters = []);
		function addParameter($key, $parameter);
		function removeParameter($parameter);
		function removeParameterAt($key);
		function hasParameter($parameter);
		function hasParameterAtKey($key);
		function getParameter($key);
		function parameters();
		/**
		 * Investigates if data exists
		 * @param string $prefix (optional) The prefix of the data column (if any)
		 */
		function emptyParameters($prefix = null);

		function reset(array $data = null);

		/**
		 * Ensure magic-getter
		 */
		function __get($name);
	}
}