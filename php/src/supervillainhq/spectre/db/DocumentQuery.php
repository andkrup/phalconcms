<?php
namespace supervillainhq\spectre\db{
	class DocumentQuery{
		static $dbAdapter;
		protected $rawStatement;

		static function create($sql, $cache = false){
			if(!isset(self::$dbAdapter)){
				throw new \Exception('Static reference to DatabaseAdapter not set. Please assign a DbAdapter to DocumentQuery::$dbAdapter');
			}
			return new DocumentQuery($sql);
		}

		private function __construct($sql){
			$this->rawStatement = $sql;
		}
	}
}