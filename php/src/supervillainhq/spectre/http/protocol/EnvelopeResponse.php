<?php
namespace supervillainhq\spectre\http\json{

	use Phalcon\Http\Response;
	use supervillainhq\core\http\json\Envelope;

	class JsonResponse extends Response{
		protected $envelope;


		function envelope(Envelope $envelope = null){
			if(is_null($envelope)){
				return $this->envelope;
			}
			$this->envelope = $envelope;
		}

		function __construct(Envelope $envelope = null, $content = null, $code = null, $status = null){
			parent::__construct($content, $code, $status);
			$this->envelope($envelope);
		}

		public function setJsonContent($content){
			if($this->envelope){
				$this->envelope->payload($content);
				$content = $this->envelope;
			}
			return parent::setJsonContent($content);
		}
	}
}