<?php
namespace supervillainhq\spectre{
	use Phalcon\DiInterface;

	/**
	 * Adds default implementations of InjectionAwareInterface
	 * @author ak
	 *
	 */
	trait DependencyInjecting{
		protected $di;

		public function setDI(DiInterface $dependencyInjector){
			$this->di = $dependencyInjector;
		}
		function getDI(){
			return $this->di;
		}
	}
}