<?php
namespace supervillainhq\spectre\auth{
	use supervillainhq\spectre\db\SqlQuery;
	use Phalcon\Security;
	use Phalcon\Session\Adapter\Files;
	use Phalcon\Di;

	class MySqlAuthenticator implements Authenticator{
		protected $security;
		protected $session;
		protected $di;

		function __construct(Security $security, Files $session, Di $di){
			$this->security = $security;
			$this->session = $session;
			$this->di = $di;
		}

		function sessionAuthed(){
			return $this->session->has('authuser');
		}

		/**
		 * Checks an authenticated users privileges
		 *
		 * @return string|number|boolean|unknown|boolean
		 */
		function isAdmin(){
			if($this->sessionAuthed()){
				$user = $this->session->get('authuser');
				$sql = "select
							count(p.id) as is_admin
						from cms_Privileges p
						left join cms_UserPrivileges up on up.privilege_id = p.id
						where up.user_id = :user
						and p.id = 1;";
				$query = SqlQuery::create($sql);
				$query->query(['user' => $user->id()]);
				return $query->fetchValue('is_admin', 'bool');
			}
			return false;
		}

		function user(){
			if($this->sessionAuthed()){
				$authUser = $this->session->get('authuser');
				// invoking the 'user' service to get a UserMapper instance
				$domainUser = $this->di->getUser(['id' => $authUser->id()])->find();
				return $domainUser;
			}
		}

		function authenticate($email, $password){
			$user;
			$sql = "select passhash, u.id, u.username, ue.email from cms_Users u
				left outer join cms_UserEmails ue on ue.user_id = u.id
				where u.activated = 1
				and ue.email = :mail
				and ue.primary = 1;";
			$query = SqlQuery::create($sql);
			$query->query([
					'mail' => $email
			]);
			$arr = $query->fetch(['passhash', 'id', 'username', 'email']);
			if(!count($arr)){
				// To protect against timing attacks. Regardless of whether a user exists or not, the script will take roughly the same amount as it will always be computing a hash.
				$this->security->hash(rand());
				return false;
			}

			if(false !== $this->security->checkHash($password, $arr->passhash)){
				$user = new AuthUser($arr);
				$this->session->set('authuser', $user);
				return true;
			}
			return false;
		}

		function signOut(){
			$this->session->remove('authuser');
		}

		function updatePassword($oldPassword, $newPassword){
			$oldHash = $this->security->hash($oldPassword, 10);
			$newHash = $this->security->hash($newPassword, 10);
		}

		function resetPassword($primaryEmail){
		}
	}
}