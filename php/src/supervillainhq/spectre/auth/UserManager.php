<?php
namespace supervillainhq\spectre\auth{
	use supervillainhq\spectre\contacts\ContactInfo;
	use Phalcon\Config;
	use Phalcon\DiInterface;
	use supervillainhq\spectre\DependencyInjecting;
	use supervillainhq\spectre\cms\User;
	use supervillainhq\spectre\db\SqlQuery;

	class UserManager{
		use DependencyInjecting;

		private $config;

		function __construct(DiInterface $dependencyInjector, Config $config){
			$this->setDI($dependencyInjector);
			$this->config = $config;
		}

		/**
		 * Create a new user
		 *
		 * @param string $username
		 * @param string $email
		 * @param string $cleartextPassword
		 * @param ContactInfo $contactInfo (optional)
		 * @param array $privileges (optional)
		 */
		function createNewUser($username, $email, $cleartextPassword, ContactInfo $contactInfo = null, array $privileges = []){

			$userWriter = $this->di->getObjectwriter('user', ['username' => trim($username)]);
			$userId = $userWriter->create();
			$userMapper = $this->di->getObjectmapper('user', ['id' => $userId]);
			$user = $userMapper->get();

			$emailWriter = $this->di->getObjectwriter('useremail', [
					'email' => $email,
					'user_id' => $userId,
					'primary' => 1
			]);
			$emailWriter->create();
			$this->updatePasshash($user, $cleartextPassword);
			return $user;
		}
		private function updatePasshash(AuthUser $user, $clearText){
			$passhash = $this->di->getSecurity()->hash($clearText);
			$sql = "update cms_Users set
					passhash = :hash
					where id = :id;";
			$parameters = [
					'id' => $user->id(),
					'hash' => $passhash,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}

		function changeUserPassword($resetPasswordToken, $cleartextPassword){
			$password = $this->security->hash($cleartextPassword);
		}

		/**
		 * Shut down a user account without deleting it
		 *
		 * @param User $user
		 * @return string A reactivation token that the real-life user can use to reactivate the account
		 */
		function deactivate(User $user){
			$reactivateToken = '';
			return $reactivateToken;
		}
		function reactivate($reactivateToken){}

		function addToGroup(UserGroup $group, AuthUser $user){}

		function removeFromGroup(UserGroup $group, AuthUser $user){}
	}
}