<?php
namespace supervillainhq\spectre\auth{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\db\DataReader;

	class Privilege{
		use DataAware;

		protected $name;
		protected $description;

		function name($name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}
		function description( $description = null){
			if(is_null($description)){
				return $this->description;
			}
			$this->description = $description;
		}

		function equals(Privilege $privilege){
			return $privilege->name() == $this->name();
		}

		static function inflate(DataReader $reader){
			$instance = new Privilege();
			$instance->id = $reader->id;
			$instance->name = $reader->name;
			$instance->description = $reader->description;
			return $instance;
		}
	}
}