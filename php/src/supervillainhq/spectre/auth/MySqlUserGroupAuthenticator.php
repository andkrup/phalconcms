<?php
namespace supervillainhq\spectre\auth{
	use supervillainhq\spectre\db\SqlQuery;

	class MySqlUserGroupAuthenticator extends MySqlAuthenticator implements Authenticator{
		/**
		 * Checks an authenticated users group-privileges. This is different from the parent class implementation that
		 * doesn't implement user-groups and group-privileges
		 *
		 * {@inheritDoc}
		 * @see \supervillainhq\spectre\auth\MySqlAuthenticator::isAdmin()
		 */
		function isAdmin(){
			if($this->sessionAuthed()){
				$user = $this->session->get('authuser');
				$sql = "select
							count(p.id) as is_admin
						from cms_UserGroupUsers ugu
						left join cms_UserGroups ug on ug.id = ugu.usergroup_id
						left join cms_UserGroupPrivileges up on up.usergroup_id = ug.id
						left join cms_Privileges p on p.id = up.privilege_id
						where ugu.user_id = :user
						and p.id = 1;";
				$query = SqlQuery::create($sql);
				$query->query(['user' => $user->id()]);
				return $query->fetchValue('is_admin', 'bool');
			}
			return false;
		}
	}
}