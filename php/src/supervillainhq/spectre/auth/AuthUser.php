<?php
namespace supervillainhq\spectre\auth{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\db\DataReader;

	/**
	 * A basic user that can be authenticated by means of some credentials
	 * @author ak
	 *
	 */
	class AuthUser{
		use DataAware;

		protected $userName;
		protected $primaryEmail;
		protected $active;
		protected $created;
		protected $updated;

		function name(){
			return $this->userName;
		}
		function email(){
			return $this->primaryEmail;
		}
		function active(){
			return $this->active;
		}

		function created(\DateTime $date = null){
			if(is_null($date)){
				return $this->created;
			}
			$this->created = $date;
		}
		function updated(\DateTime $date = null){
			if(is_null($date)){
				return $this->updated;
			}
			$this->updated = $date;
		}

		function __construct($data = null){
			if(is_object($data)){
				$data = (object) $data;
				$this->id = intval($data->id);
				$this->primaryEmail = trim($data->email);
				$this->userName = trim($data->username);
			}
		}

		static function inflate(DataReader $reader){
			if($reader->emptyParameters('user')){
				return null;
			}
			$instance = new AuthUser();
			$instance->id = $reader->id;
			$instance->primaryEmail = $reader->email;
			$instance->userName = $reader->username;
			return $instance;
		}
	}
}