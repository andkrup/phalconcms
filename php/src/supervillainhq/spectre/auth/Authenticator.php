<?php
namespace supervillainhq\spectre\auth{
	interface Authenticator{
		function authenticate($email, $password);
	}
}