<?php
namespace supervillainhq\spectre\auth\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;

	class AuthUserMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof AuthUser){
					$this->addParameter('id', $data->id());
					$this->addParameter('username', $data->name());
					$this->addParameter('email', $data->email());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('user_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$searchables = ['id', 'username'];
		    while (count($searchables) > 0){
		    	$search = array_shift($searchables);
		    	if($this->hasParameterAtKey($search)){
		    		$value = $this->getParameter($search);
	    			$sql = "select
								u.id as user_id, u.username as user_username
							from cms_Users u
							where u.{$search} = :{$search};";
	    			$query = SqlQuery::create($sql);
	    			$query->query(["{$search}" => $value]);
	    			$row = $query->fetch();
	    			if(isset($row)){
	    				$mapper = new AuthUserMapper((array) $row);
		    			$user = $mapper->inflate();
	    				return $user;
	    			}
	    		}
    		}
			return null;
		}
		function get(){}
		function exists(){}
		function all(){
		}
		function reset(array $data = null){}

		function inflate(){
			return AuthUser::create($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'username':
					case 'email':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}