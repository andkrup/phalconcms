<?php
namespace supervillainhq\spectre\auth\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\DependencyInjecting;
	use supervillainhq\spectre\auth\Privilege;

	class PrivilegeWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof Privilege){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
					$this->addParameter('description', $data->description());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('privilege_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into cms_Privileges
					(name, description)
					values (:name, description);";
			$parameters = [
					'name' => $this->name,
					'description' => $this->description
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return intval($query->lastInsertId());
		}

		function update(){
			$sql = "update cms_Privileges set
					name = :name,
					description = description
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'name' => $this->name,
					'description' => $this->description
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}

		function delete(){
			$sql = "delete from cms_Privileges
					where id = :id;";
			$query = SqlQuery::create($sql);
			return $query->execute(['id' => $this->id]);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'name':
					case 'description':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}