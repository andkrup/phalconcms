<?php
namespace supervillainhq\spectre\auth\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\auth\Privilege;

	class PrivilegeMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof Privilege){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
					$this->addParameter('description', $data->description());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('privilege_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$sql = "select
						p.id as privilege_id, p.name as privilege_name, p.description as privilege_description
					from cms_Privileges p
					where p.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			$row = $query->fetch();
			if($row){
				$mapper = $this->getDI()->getObjectmapper('privilege', (array) $row);
				return $mapper->inflate();
			}
			return null;
		}

		function get(){
			return $this->find();
		}

		function exists(){
			$sql = "select 1 as existing
					from cms_Privileges p
					where p.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			return $query->fetchValue('existing', SqlQuery::TYPE_BOOLEAN);
		}

		function all(){
			$sql = "select
						p.id as privilege_id, p.name as privilege_name, p.description as privilege_description
					from cms_Privileges p;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$privileges = [];
			foreach ($rows as $row){
				$mapper = $this->getDI()->getObjectmapper('privilege', (array) $row);
				if($privilege = $mapper->inflate()){
					array_push($privileges, $privilege);
				}
			}
			return $privileges;
		}

		function reset(array $data = null){
			$this->resetParameters($data);
		}

		function inflate(){
			return Privilege::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'name':
					case 'description':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}