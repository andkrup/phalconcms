<?php
namespace supervillainhq\spectre\cms{
	use Phalcon\Mvc\View;
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\Loader;
	use Phalcon\DiInterface;
	use Phalcon\Mvc\ModuleDefinitionInterface;

	class PhalconCms implements ModuleDefinitionInterface{
		static $viewDir = '../app/views/';

		public function registerAutoloaders(DiInterface $dependencyInjector=null){
			$loader = new Loader();
			$loader->registerNamespaces([
					'supervillainhq\spectre\cms\controllers' => '../vendor/supervillainhq/phpalconcms/php/src/supervillainhq/spectre/cms/controllers/',
			]);
			$loader->register();
		}
		public function registerServices(DiInterface $dependencyInjector){
			//Registering a dispatcher
			$dependencyInjector->set('dispatcher', function() {
				$dispatcher = new Dispatcher();
				$dispatcher->setDefaultNamespace('supervillainhq\spectre\cms\controllers');
				return $dispatcher;
			});

			//Registering the view component
			$dependencyInjector->set('view', function() use($dependencyInjector){
				$config = $dependencyInjector->getConfig();
				$view = new View();
				$view->setViewsDir($config->phalconcms->viewsDir);
				return $view;
			});
		}

		static function getCmsMappers(){
			return [
					'page' => 'supervillainhq\spectre\cms\db\PageMapper',
					'section' => 'supervillainhq\spectre\cms\db\SectionMapper',
					'template' => 'supervillainhq\spectre\cms\db\TemplateMapper',
					'asset' => 'supervillainhq\spectre\cms\db\AssetMapper',
					'slide' => 'supervillainhq\spectre\cms\components\db\SliderMapper',
					'post' => 'supervillainhq\spectre\posting\db\NewsPostMapper',
					'user' => 'supervillainhq\spectre\cms\users\db\UserMapper',
					'useremail' => 'supervillainhq\spectre\cms\users\db\UserEmailMapper',
					'userphone' => 'supervillainhq\spectre\cms\users\db\UserPhoneMapper',
					'privilege' => 'supervillainhq\spectre\auth\db\PrivilegeMapper',
					'country' => 'supervillainhq\spectre\intl\db\CountryMapper',
			];
		}
		static function getCmsWriters(){
			return [
					'page' => 'supervillainhq\spectre\cms\db\PageWriter',
					'section' => 'supervillainhq\spectre\cms\db\SectionWriter',
					'template' => 'supervillainhq\spectre\cms\db\TemplateWriter',
					'asset' => 'supervillainhq\spectre\cms\db\AssetWriter',
					'slide' => 'supervillainhq\spectre\cms\components\db\SliderWriter',
					'post' => 'supervillainhq\spectre\posting\db\NewsPostWriter',
					'user' => 'supervillainhq\spectre\cms\users\db\UserWriter',
					'useremail' => 'supervillainhq\spectre\cms\users\db\UserEmailWriter',
					'userphone' => 'supervillainhq\spectre\cms\users\db\UserPhoneWriter',
					'privilege' => 'supervillainhq\spectre\auth\db\PrivilegeWriter',
					'country' => 'supervillainhq\spectre\cms\intl\db\CountryWriter',
			];
		}
	}
}