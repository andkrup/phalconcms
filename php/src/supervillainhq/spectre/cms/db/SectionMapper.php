<?php
namespace supervillainhq\spectre\cms\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\cms\Section;

	class SectionMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof Section){
					$this->addParameter('id', $data->id());
					$this->addParameter('title', $data->title());
					$this->addParameter('html', $data->html());
					$this->addParameter('markup', $data->markup());
					$this->addParameter('raw', $data->raw());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('section_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
// 			$sql = "select
// 						p.name as page_name, p.title as page_title, p.slug as page_slug, p.section_manager as page_section_manager
// 						, t.id as template_id, t.name as template_name, t.filepath as template_filepath
// 					from cms_Pages p
// 					left join cms_Templates t on t.id = p.template_id
// 					where p.id = :id;";
			$sql = "select
						s.id as section_id, s.title as section_title, s.content_html as section_content_html
					from cms_Sections s
					where s.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			$row = $query->fetch();
			if($row){
				$sectionMapper = $this->getDI()->getObjectmapper('template', (array) $row);
				$section = $sectionMapper->inflate();
				return $section;
			}
			return null;
		}

		function get(){
			return $this->find();
		}

		function exists(){
			$sql = "select 1 as existing
					from cms_Sections s
					where s.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			return $query->fetchValue('existing', SqlQuery::TYPE_BOOLEAN);
		}

		function all(){
			$sql = "select
						p.name as page_name, p.title as page_title, p.slug as page_slug, p.section_manager as page_section_manager
						, t.id as template_id, t.name as template_name, t.filepath as template_filepath
					from cms_Pages p
					left join cms_Templates t on t.id = p.template_id;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$pages = [];
			foreach ($rows as $row){
				$pageMapper = $this->getDI()->getObjectmapper('page', (array) $row);
				if($page = $pageMapper->inflate()){
					$templateMapper = $this->getDI()->getObjectmapper('template', (array) $row);
					$page->template($templateMapper->inflate());
					array_push($pages, $page);
				}
			}
			return $pages;
		}

		function reset(array $data = null){
			$this->resetParameters($data);
		}

		function inflate(){
			return Section::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
// 					case 'template':
						return intval($this->getParameter($name));
					case 'title':
					case 'html':
					case 'content_html':
					case 'raw':
					case 'content_raw':
					case 'markup':
					case 'content_markup':
						return stripslashes(trim($this->getParameter($name)));
					case 'created':
					case 'updated':
						return \DateTime::createFromFormat('Y-m-d H:i:s', $this->getParameter($name));
				}
			}
			return null;
		}
	}
}