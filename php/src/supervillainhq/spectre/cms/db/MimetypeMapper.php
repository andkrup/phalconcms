<?php
namespace supervillainhq\spectre\cms\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\assets\Mimetype;
	use supervillainhq\spectre\db\Mapper;

	class MimetypeMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof Mimetype){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
					$this->addParameter('extension', $data->extension());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('mimetype_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$searchables = ['id', 'extension', 'mimetype'];
			while (count($searchables) > 0){
				$search = array_shift($searchables);
				if($this->hasParameterAtKey($search)){
					$value = $this->getParameter($search);
					$sql = "select
								mt.id as mimetype_id, mt.mimetype as mimetype_name, mt.extension as mimetype_extension
								, et.name as editortype_name
								, e.name as editor_name
							from cms_Mimetypes mt
							left join cms_EditorTypes et on et.id = mt.editortype_id
							left join cms_Editors e on e.id = et.editor_id
							where mt.{$search} = :value;";
					$query = SqlQuery::create($sql);
					$query->query(['value' => $value]);
					$rows = $query->fetchAll();
					$types = [];
					if(isset($rows)){
						foreach ($rows as $row){
							$mapper = $mapper = new AssetMapper((array) $row);;
							$type = $mapper->inflate();
							if(!is_null($type)){
								array_push($types, $type);
							}
						}
					}
					if(count($types) == 1){
						return $types[0];
					}
					return $types;
				}
			}
			return null;
		}
		function get(){
			return $this->find();
		}
		function exists(){}
		function all(){
			$sql = "select
						a.id as asset_id, a.name as asset_name, a.src as asset_filepath
						, mt.mimetype as asset_mimetype
					from cms_Assets a
					left join cms_Mimetypes mt on mt.id = a.mimetype_id;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$assets = [];
			foreach ($rows as $row){
				$mapper = new AssetMapper((array) $row);
				$asset = $mapper->inflate();
				if(!is_null($asset)){
					array_push($assets, $asset);
				}
			}
			return $assets;
		}
		function reset(array $data = null){}

		function inflate(){
			return Mimetype::create($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'name':
					case 'extension':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}