<?php
namespace supervillainhq\spectre\cms\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\Page;
	use supervillainhq\spectre\DependencyInjecting;

	class PageWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof Page){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
					$this->addParameter('title', $data->title());
					$this->addParameter('slug', $data->slug());
					$this->addParameter('description', $data->description());
					$this->addParameter('template', $data->template()->id());
					$this->addParameter('section_manager', get_class($data->sectionManager()));
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('page_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into cms_Pages
					(name, title, slug, description, template_id, section_manager, created)
					values (:name, :title, :slug, :description, :template, :sectionmanager, now());";
			$parameters = [
					'name' => $this->name,
					'title' => $this->title,
					'slug' => $this->slug,
					'description' => $this->description,
					'template' => $this->template,
					'sectionmanager' => $this->section_manager,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}
		function update(){
			$sql = "update cms_Pages set
					name = :name,
					title = :title,
					slug = :slug,
					description = :description,
					template_id = :template,
					section_manager = :sectionmanager,
					updated = now()
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'name' => $this->name,
					'title' => $this->title,
					'slug' => $this->slug,
					'template' => $this->template,
					'sectionmanager' => $this->section_manager,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}
		function delete(){}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
					case 'template':
					case 'template_id':
						return intval($this->getParameter($name));
					case 'name':
					case 'title':
					case 'slug':
					case 'description':
						return stripslashes(trim($this->getParameter($name)));
					case 'section_manager':
// 						return stripslashes(trim($this->getParameter($name)));
						return null;
				}
			}
			return null;
		}
	}
}