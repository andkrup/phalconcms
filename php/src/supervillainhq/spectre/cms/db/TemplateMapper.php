<?php
namespace supervillainhq\spectre\cms\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\Template;
	use supervillainhq\spectre\db\Mapper;

	class TemplateMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof Template){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
					$this->addParameter('filepath', $data->filepath());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('template_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$sql = "select
						t.id as template_id, t.name as template_name, t.filepath as template_filepath
					from cms_Templates t
					where t.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			$rows = $query->fetchAll();
			$page;
			foreach ($rows as $row){
				$templateMapper = $this->getDI()->getObjectmapper('template', (array) $row);
				$template = $templateMapper->inflate();
				return $template;
			}
			return null;
		}

		function get(){
			return $this->find();
		}

		function exists(){
			$sql = "select 1 as existing
					from cms_Templates t
					where t.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			return $query->fetchValue('existing', SqlQuery::TYPE_BOOLEAN);
		}

		function all(){
			$sql = "select
						t.id as template_id, t.name as template_name, t.filepath as template_filepath
					from cms_Templates t;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$templates = [];
			foreach ($rows as $row){
				$templateMapper = $this->getDI()->getObjectmapper('template', (array) $row);
				array_push($templates, $templateMapper->inflate());
			}
			return $templates;
		}

		function reset(array $data = null){}

		function inflate(){
			return Template::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'name':
					case 'filepath':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}