<?php
namespace supervillainhq\spectre\cms\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\assets\FileAsset;
	use supervillainhq\spectre\cms\assets\Mimetype;
	use supervillainhq\spectre\DependencyInjecting;

	class AssetWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof FileAsset){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
					$this->addParameter('filepath', $data->filepath());
					$this->addParameter('mimetype', $data->mimetype());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('asset_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into cms_Assets
					(mimetype_id, name, src)
					values (:mimetype, :name, :src);";
			$parameters = [
					'mimetype' => $this->mimetype,
					'name' => $this->name,
					'src' => $this->filepath,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}
		function update(){
			$sql = "update cms_Assets set
					name = :name
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'name' => $this->name,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}
		function delete(){}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'name':
					case 'filepath':
						return stripslashes(trim($this->getParameter($name)));
					case 'mimetype':
						$value = $this->getParameter($name);
						if($value instanceof Mimetype){
							return $value->id();
						}
						return intval($value);
				}
			}
			return null;
		}
	}
}