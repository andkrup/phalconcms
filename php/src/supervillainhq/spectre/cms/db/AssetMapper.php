<?php
namespace supervillainhq\spectre\cms\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\assets\FileAsset;
	use supervillainhq\spectre\cms\assets\Mimetype;
	use supervillainhq\spectre\db\Mapper;

	class AssetMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof FileAsset){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
					$this->addParameter('mimetype', $data->mimetype());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('asset_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$sql = "select
						a.id as asset_id, a.name as asset_name, a.src as asset_filepath
						, mt.id as mimetype_id, mt.mimetype as mimetype_name, mt.extension as mimetype_extension
					from cms_Assets a
					left join cms_Mimetypes mt on mt.id = a.mimetype_id
					where a.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			$row = $query->fetch();
			if(isset($row)){
				$asset = FileAsset::create(new AssetMapper((array) $row));
				$asset->mimetype(Mimetype::create(new MimetypeMapper((array) $row)));
				return $asset;
			}
			return null;
		}
		function get(){
			return $this->find();
		}
		function exists(){}
		function all(){
			$sql = "select
						a.id as asset_id, a.name as asset_name, a.src as asset_filepath
						, mt.mimetype as asset_mimetype
					from cms_Assets a
					left join cms_Mimetypes mt on mt.id = a.mimetype_id;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$assets = [];
			foreach ($rows as $row){
				$mapper = new AssetMapper((array) $row);
				$asset = $mapper->inflate();
				if(!is_null($asset)){
					array_push($assets, $asset);
				}
			}
			return $assets;
		}
		function reset(array $data = null){}

		function inflate(){
			return FileAsset::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'name':
					case 'filepath':
						return stripslashes(trim($this->getParameter($name)));
					case 'mimetype':
						return $this->getParameter($name);
				}
			}
			return null;
		}
	}
}