<?php
namespace supervillainhq\spectre\cms\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\Page;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\cms\SimpleSectionManager;

	class PageMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof Page){
					$this->addParameter('id', $data->id());
					$this->addParameter('locale', $data->locale());
					$this->addParameter('name', $data->name());
					$this->addParameter('title', $data->title());
					$this->addParameter('slug', $data->slug());
					$this->addParameter('description', $data->description());
					$this->addParameter('created', $data->slug());
					$this->addParameter('updated', $data->slug());
					$this->addParameter('template', $data->template()->id());
					$this->addParameter('section_manager', get_class($data->sectionManager()));
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('page_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
// 			$sql = "select
// 						p.name as page_name, p.title as page_title, p.slug as page_slug, p.section_manager as page_section_manager
// 						, t.id as template_id, t.name as template_name, t.filepath as template_filepath
// 					from cms_Pages p
// 					left join cms_Templates t on t.id = p.template_id
// 					where p.id = :id;";
			$searchables = ['id', 'name', 'title', 'slug'];
	    	while (count($searchables) > 0){
	    		$search = array_shift($searchables);
	    		if($this->hasParameterAtKey($search)){
	    			$value = $this->getParameter($search);
					$sql = "select
								p.id as page_id, p.name as page_name, p.title as page_title, p.slug as page_slug, p.description as page_description, p.section_manager as page_section_manager, p.created as page_created, p.updated as page_updated
								, t.id as template_id, t.name as template_name, t.filepath as template_filepath
								, s.id as section_id, s.title as section_title, s.content_html as section_content_html
							from cms_Pages p
							left join cms_PageSections ps on ps.page_id = p.id
							left join cms_Sections s on s.id = ps.section_id
							left join cms_Templates t on t.id = p.template_id
							where p.{$search} = :{$search}
							order by ps.display_order;";
					$query = SqlQuery::create($sql);
					$query->query(["{$search}" => $value]);
					$rows = $query->fetchAll();
					$page = null;
					foreach ($rows as $row){
						$templateMapper = $this->getDI()->getObjectmapper('template', (array) $row);
						$sectionMapper = $this->getDI()->getObjectmapper('section', (array) $row);
						if(!isset($page)){
							$pageMapper = $this->getDI()->getObjectmapper('page', (array) $row);
							$page = $pageMapper->inflate();
							$page->template($templateMapper->inflate());
						}
						$section = $sectionMapper->inflate();
						$page->sectionManager()->addSection($section);
						return $page;
					}
					return null;
	    		}
	    	}
	    	return null;
		}

		function get(){
			return $this->find();
		}

		function exists(){
			$sql = "select 1 as existing
					from cms_Pages p
					where p.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			return $query->fetchValue('existing', SqlQuery::TYPE_BOOLEAN);
		}

		function all(){
			$sql = "select
						p.id as page_id, p.name as page_name, p.title as page_title, p.slug as page_slug, p.description as page_description, p.section_manager as page_section_manager, p.created as page_created, p.updated as page_updated
						, t.id as template_id, t.name as template_name, t.filepath as template_filepath
						, s.id as section_id, s.title as section_title, s.content_html as section_content_html
					from cms_Pages p
					left join cms_PageSections ps on ps.page_id = p.id
					left join cms_Sections s on s.id = ps.section_id
					left join cms_Templates t on t.id = p.template_id
					order by ps.display_order;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$pages = [];
			$current = null;
			foreach ($rows as $row){
				$templateMapper = $this->getDI()->getObjectmapper('template', (array) $row);
				$sectionMapper = $this->getDI()->getObjectmapper('section', (array) $row);
				$pageMapper = $this->getDI()->getObjectmapper('page', (array) $row);
				$page = $pageMapper->inflate();
				$page->template($templateMapper->inflate());
				if(!isset($current) || !$current->equals($page)){
					$current = $page;
					array_push($pages, $current);
				}
				$section = $sectionMapper->inflate();
				$current->sectionManager()->addSection($section);
			}
			return $pages;
		}

		function reset(array $data = null){}

		function inflate(){
			return Page::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
					case 'template':
					case 'template_id':
						return intval($this->getParameter($name));
					case 'locale':
					case 'name':
					case 'slug':
					case 'title':
					case 'description':
						return stripslashes(trim($this->getParameter($name)));
					case 'created':
					case 'updated':
						return \DateTime::createFromFormat('Y-m-d H:i:s', $this->getParameter($name));
					case 'section_manager':
						$classpath = $this->getParameter($name);
						if(is_null($classpath)){
							return new SimpleSectionManager();
						}
						return new SimpleSectionManager();
				}
			}
			return null;
		}
	}
}