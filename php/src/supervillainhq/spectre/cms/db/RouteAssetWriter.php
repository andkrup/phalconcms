<?php
namespace supervillainhq\spectre\cms\db{
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\cms\assets\FileAsset;
	use supervillainhq\spectre\DependencyInjecting;

	class RouteAssetWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($uri, FileAsset $asset){
			$this->resetParameters();
			$this->addParameter('uri', $uri);
			$this->addParameter('asset', $asset->id());
		}
		function create(){
			$sql = "insert into cms_RequestAssets
					(route, asset_id)
					values (:uri, :asset);";
			$parameters = [
					'uri' => $this->uri,
					'asset' => $this->asset,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}
		function update(){
			$sql = "update cms_RequestAssets set
					name = :name
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'name' => $this->name,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}
		function delete(){}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
					case 'asset':
						return intval($this->getParameter($name));
					case 'uri':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}