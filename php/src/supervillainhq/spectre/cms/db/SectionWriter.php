<?php
namespace supervillainhq\spectre\cms\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\Section;
	use supervillainhq\spectre\DependencyInjecting;

	class SectionWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof Section){
					$this->addParameter('id', $data->id());
					$this->addParameter('title', $data->title());
					$this->addParameter('html', $data->html());
					$this->addParameter('markup', $data->markup());
					$this->addParameter('raw', $data->raw());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('section_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into cms_Sections
					(title, content_raw, content_html, content_markup)
					values (:title, :raw, :html, :markup);";
			$parameters = [
					'title' => $this->title,
					'content_raw' => $this->content_raw,
					'content_html' => $this->content_html,
					'content_markup' => $this->content_markup,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}

		function update(){
			$sql = "update cms_Sections set
					title = :title,
					content_raw = :content_raw,
					content_html = :content_html,
					content_markup = :content_markup
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'title' => $this->title,
					'content_raw' => $this->content_raw,
					'content_html' => $this->content_html,
					'content_markup' => $this->content_markup,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}

		function delete(){
			$sql = "delete from cms_Sections
					where id = :id;";
			$query = SqlQuery::create($sql);
			return $query->execute(['id' => $this->id]);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'name':
					case 'filepath':
						return stripslashes(trim($this->getParameter($name)));
					case 'mimetype':
						$value = $this->getParameter($name);
						if($value instanceof Mimetype){
							return $value->id();
						}
						return intval($value);
				}
			}
			return null;
		}
	}
}