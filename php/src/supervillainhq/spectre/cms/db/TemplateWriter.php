<?php
namespace supervillainhq\spectre\cms\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\Template;
	use supervillainhq\spectre\DependencyInjecting;

	class TemplateWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof Template){
					$this->addParameter('id', $data->id());
					$this->addParameter('name', $data->name());
					$this->addParameter('filepath', $data->filepath());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('template_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into cms_Templates
					(name, filepath)
					values (:name, :filepath);";
			$parameters = [
					'name' => $this->name,
					'filepath' => $this->filepath,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}
		function update(){
			$sql = "update cms_Templates set
					name = :name,
					filepath = :filepath
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'name' => $this->name,
					'filepath' => $this->filepath,
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}
		function delete(){}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'name':
					case 'filepath':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}