<?php
namespace supervillainhq\spectre\cms{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\db\DataReader;

	class Template{
		use DataAware;

		protected $name;
		protected $filepath;

		function name($name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}
		function filepath($filepath = null){
			if(is_null($filepath)){
				return $this->filepath;
			}
			$this->filepath = $filepath;
		}
		function templatePath(){
			return str_replace('.phtml', '', $this->filepath);
		}

		static function inflate(DataReader $reader){
			$instance = new Template();
			$instance->id = $reader->id;
			$instance->name = $reader->name;
			$instance->filepath = $reader->filepath;
			return $instance;
		}

		function __toString(){
			return "Template template";
		}
	}
}