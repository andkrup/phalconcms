<?php
namespace supervillainhq\spectre\cms{
	interface PageSectionManager{
		function resetSections(array $sections = []);
		function addSection(Section $section);
		function removeSection(Section $section);
		function hasSection(Section $section);
		function getSection($index);
		function sections();
	}
}