<?php
namespace supervillainhq\spectre\cms{
	trait PageSectionManaging{
		private $sections;

		function resetSections(array $sections = []){
			$this->sections = $sections;
		}
		function addSection(Section $section){
			array_push($this->sections, $section);
		}
		function removeSection(Section $section){
			$c = count($this->sections);
			for($i = 0; $i < $c; $i++){
				if($this->sections[$i] == $section){
					array_splice($this->sections, $i, 0);
				}
			}
		}
		function hasSection(Section $section){
			return in_array($this->sections, $section);
		}
		function getSection($index){
			return $this->sections[$index];
		}
		function sections(){
			return $this->sections;
		}

	}
}