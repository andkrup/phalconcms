<?php
namespace supervillainhq\spectre\cms{
	use supervillainhq\core\db\DataAware;
	use Phalcon\Config;

	trait Editing{
		use DataAware;

		private $name;
		private $config;
		private $classpath;

		function name( $name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}
		function classpath($path = null){
			if(is_null($path)){
				return $this->classpath;
			}
			$this->classpath = $path;
		}
		function config(Config $config = null){
			if(is_null($config)){
				return $this->config;
			}
			$this->config = $config;
		}
	}
}