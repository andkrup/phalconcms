<?php
namespace supervillainhq\spectre\cms\backend{

	trait Manageable{
		protected $dataManager;

		function dataManager(DataManager $dataManager = null){
			if(is_null($dataManager)){
				return $this->dataManager;
			}
			$this->dataManager = $dataManager;
		}

		function update(){
			if(isset($this->dataManager)){
				return $this->dataManager->update();
			}
			return null;
		}
		function remove(){
			if(isset($this->dataManager)){
				return $this->dataManager->remove();
			}
			return null;
		}
		function create(){
			if(isset($this->dataManager)){
				return $this->dataManager->create();
			}
			return null;
		}
	}
}