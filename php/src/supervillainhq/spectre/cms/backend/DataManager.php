<?php
namespace supervillainhq\spectre\cms\backend{
	/**
	 * Managers can modify data and implements the C,U & D functions expected of a
	 * CRUD application (where the Read functions are implemented by the frontend
	 * classes). This way, we can expose only the read functions to the frontend
	 *
	 * @author ak
	 *
	 */
	interface DataManager{
		function update();
		function remove();
		function create();
	}
}