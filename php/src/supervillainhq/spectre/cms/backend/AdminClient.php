<?php
namespace supervillainhq\spectre\cms\backend{
	/**
	 * Facilitates other objects creating/updating/deleting data
	 *
	 * @author ak
	 *
	 */
	class AdminClient extends CmsClient{
		function loadPage($name){
			$page = new Page();
			// set up concrete SectionManager
			$sectionManager = new PageSectionRotator();
			$page->sectionManager($sectionManager);
			// set up admin helper for managerial functions (the crud functionality)
			$dataHelper = new PageAdmin();
			$page->dataHelper($dataHelper);
			return $page;
		}
	}
}