<?php
namespace supervillainhq\spectre\cms\backend{
	interface IsManageable extends DataManager{
		function dataManager(DataManager $dataManager = null);
	}
}