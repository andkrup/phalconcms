<?php
namespace supervillainhq\spectre\cms{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\db\DataReader;

	class Page implements PageSectionManager{
		use DataAware;

		protected $locale;
		protected $name;
		protected $slug;
		protected $title;
		protected $description;
		protected $template;
		protected $sectionManager;
		protected $created;
		protected $updated;

		function sectionManager(PageSectionManager $sectionManager = null){
			if(is_null($sectionManager)){
				return $this->sectionManager;
			}
			$this->sectionManager = $sectionManager;
		}
		function id(){
			return $this->id;
		}
		function locale(\Locale $locale = null){
			if(is_null($locale)){
				return $this->locale;
			}
			$this->locale = $locale;
		}
		function name($name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}
		function slug($slug = null){
			if(is_null($slug)){
				return $this->slug;
			}
			$this->slug = $slug;
		}
		function title($title = null){
			if(is_null($title)){
				return $this->title;
			}
			$this->title = $title;
		}
		function description( $description = null){
			if(is_null($description)){
				return $this->description;
			}
			$this->description = $description;
		}
		function template(Template $template = null){
			if(is_null($template)){
				return $this->template;
			}
			$this->template = $template;
		}
		function created(\DateTime $date = null){
			if(is_null($date)){
				return $this->created;
			}
			$this->created = $date;
		}
		function updated(\DateTime $date = null){
			if(is_null($date)){
				return $this->updated;
			}
			$this->updated = $date;
		}

		function resetSections(array $sections = []){
			return $this->sectionManager()->resetSections($sections);
		}
		function addSection(Section $section){
			return $this->sectionManager()->addSection($section);
		}
		function removeSection(Section $section){
			return $this->sectionManager()->removeSection($section);
		}
		function hasSection(Section $section){
			return $this->sectionManager()->hasSection($section);
		}
		function getSection($index){
			return $this->sectionManager()->getSection($index);
		}
		function sections(){
			return $this->sectionManager()->sections();
		}

		function contents(){
			return $this->sectionManager()->sections();
		}

		static function inflate(DataReader $reader){
			$instance = new Page();
			$instance->id = $reader->id;
			$instance->name = $reader->name;
			$instance->slug = $reader->slug;
			$instance->title = $reader->title;
			$instance->description = $reader->description;
			$instance->sectionManager = $reader->section_manager;
			$instance->created = $reader->created;
			$instance->updated = $reader->updated;
			return $instance;
		}

		function equals(Page $page){
			return $page->id() == $this->id();
		}
	}
}