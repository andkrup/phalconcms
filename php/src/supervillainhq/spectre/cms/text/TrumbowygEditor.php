<?php
namespace supervillainhq\spectre\cms\text{
	class TrumbowygEditor extends TextEditor{

		function dependencies(){
			$this->requireHeadScripts(['trumbowyg']);
			$this->requireStylesheets(['trumbowyg']);
		}

		function init(){
			echo <<<EDITOR
<script>
$(document).ready(function(){
	$('textarea').trumbowyg();
});
</script>
EDITOR;
		}
	}
}