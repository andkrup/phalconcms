<?php
namespace supervillainhq\spectre\cms\commands{
	use supervillainhq\spectre\Command;
	use Phalcon\Http\Request\File;
	use supervillainhq\spectre\cms\db\AssetMapper;
	use supervillainhq\spectre\cms\db\AssetWriter;
	use supervillainhq\spectre\cms\db\RouteAssetWriter;
	use supervillainhq\spectre\cms\assets\FileAsset;
	use supervillainhq\spectre\cms\assets\Mimetype;
	use supervillainhq\spectre\cms\db\MimetypeMapper;

	class AddAssetToRouteCommand implements Command{
		protected $uri;
		protected $id;

		function __construct($uri, $id){
			$this->uri = $uri;
			$this->id = intval($id);
		}

		function execute(){
			$mapper = new AssetMapper(['id' => $this->id]);
			$asset = $mapper->get();
			$writer = new RouteAssetWriter($this->uri, $asset);
			$insertId = $writer->create();
			return intval($insertId) > 0;
		}

		private function getAllowedMimetype($file){
			$fi = new \finfo(FILEINFO_MIME);
			$mimetype = $fi->file($file);
			foreach ($this->allowedMimetypes as $extension => $allowedMimetype){
				if(strpos($mimetype, $allowedMimetype) !== false){
					$mapper = new MimetypeMapper(['extension' => $extension]);
					return $mapper->find();
// 					return Mimetype::getByType($allowedMimetype);
				}
			}
			return null;
		}
	}
}