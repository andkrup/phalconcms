<?php
namespace supervillainhq\spectre\cms\commands{
	use supervillainhq\spectre\Command;
	use Phalcon\Http\Request\File;
	use supervillainhq\spectre\cms\db\AssetMapper;
	use supervillainhq\spectre\cms\db\AssetWriter;
	use supervillainhq\spectre\cms\assets\FileAsset;
	use supervillainhq\spectre\cms\assets\Mimetype;
	use supervillainhq\spectre\cms\db\MimetypeMapper;

	class UploadAssetCommand implements Command{
		protected $file;
		protected $tempPath;
		protected $basePath;
		protected $targetPath;
		protected $publicPath;
		protected $allowedMimetypes;

		function __construct($config, File $file, $publicPath = '/', $targetPath = null, array $allowedMimetypes = []){
			$this->file = $file;
			$this->basePath = property_exists($config, 'baseDir') ? $config->baseDir : '/vagrant_files';
			$this->tempPath = property_exists($config, 'tempDir') ? $config->tempDir : '/tmp';
			$this->publicPath = $publicPath;
			$this->targetPath = is_null($targetPath) ? $this->basePath : $targetPath;
			if(property_exists($config, 'allowedMimes')){
				$allowedMimetypes = $config->allowedMimes;
			}
			$this->allowedMimetypes = $allowedMimetypes;
		}

		function execute(){
			$mimetype = null;
			$tmpFilepath = $this->file->getTempName();
			if(!is_null($tmpFilepath) && is_readable($tmpFilepath)){
				if($mimetype = $this->getAllowedMimetype($tmpFilepath)){
					$filename = $this->file->getName();
					$targetPath = "{$this->targetPath}/{$filename}";

					if(!is_writable($this->targetPath)){
						throw new \Exception("unable to write to path '{$targetPath}'");
					}
					if($this->file->moveTo($targetPath)){
						$parameters = [
								'name' => $filename,
								'filepath' => $targetPath,
								'mimetype' => $mimetype,
						];
						$asset = FileAsset::create(new AssetMapper($parameters));
						$writer = new AssetWriter($asset);
				    	$id = $writer->create();
				    	$asset->id($id);
				    	return $asset;
					}
				}
			}
			return null;
		}

		private function getAllowedMimetype($file){
			$fi = new \finfo(FILEINFO_MIME);
			$mimetype = $fi->file($file);
			foreach ($this->allowedMimetypes as $extension => $allowedMimetype){
				if(strpos($mimetype, $allowedMimetype) !== false){
					$mapper = new MimetypeMapper(['extension' => $extension]);
					return $mapper->find();
				}
			}
			return null;
		}
	}
}