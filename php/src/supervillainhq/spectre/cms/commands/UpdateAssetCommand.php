<?php
namespace supervillainhq\spectre\cms\commands{
	use Phalcon\Http\Request\File;
	use supervillainhq\spectre\Command;
	use supervillainhq\spectre\cms\db\AssetMapper;
	use supervillainhq\spectre\cms\db\AssetWriter;
	use supervillainhq\spectre\cms\assets\FileAsset;
	use supervillainhq\spectre\cms\assets\Mimetype;
	use supervillainhq\spectre\cms\db\MimetypeMapper;

	class UpdateAssetCommand implements Command{
		protected $id;
		protected $parameters;

		function __construct($id, array $parameters){
			$this->id = $id;
			$this->parameters = $parameters;
		}

		function execute(){
			$this->parameters['id'] = $this->id;
			$writer = new AssetWriter($this->parameters);
			$writer->update();
		}

		private function getAllowedMimetype($file){
			$fi = new \finfo(FILEINFO_MIME);
			$mimetype = $fi->file($file);
			foreach ($this->allowedMimetypes as $extension => $allowedMimetype){
				if(strpos($mimetype, $allowedMimetype) !== false){
					$mapper = new MimetypeMapper(['extension' => $extension]);
					return $mapper->find();
// 					return Mimetype::getByType($allowedMimetype);
				}
			}
			return null;
		}
	}
}