<?php
namespace supervillainhq\spectre\cms{
	use Phalcon\Mvc\User\Plugin;
	use Phalcon\Events\Event;
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\Mvc\Router;
	use Phalcon\DI\FactoryDefault;
	use Phalcon\Mvc\ViewInterface;
	use supervillainhq\spectre\cms\assets\Images;

	/**
	 * Facilitates reading data from storage/cache without defining any
	 * 'admin' functionality that will not be used anyway in the frontend
	 *
	 * @author ak
	 *
	 */
	class CmsClient extends Plugin{

		function loadPage($slug){
			$pageMapper = $this->getDI()->getObjectmapper('page', ['slug' => $slug]);
			$page = $pageMapper->find();
			return $page;
		}

		public function loadImages(ViewInterface $view){
			$uri = $_SERVER['REQUEST_URI'];
			$images = Images::getAssets($uri);
			if(!empty($images)){
				$view->setVar('images', $images);
			}
		}

		public function loadAvailableImages(ViewInterface $view){
			$availableImages = Images::imageAssets();
			$view->setVar('availableImages', $availableImages);
		}

		public function beforeDispatch(Event $event, Dispatcher $dispatcher){
		}
		/**
		 * Intercepts the exception for no controller available. In here we should be able to load
		 * dynamic pages. TODO: seems like the wrong place to do this, but it also seems to be wrong
		 * to put up a very broad regexp-route for urls that map to dynamic pages. Maybe we should
		 * design a custom Router that can execute the required code before throwing the exception/
		 * dispatching the event that we're hooking into.
		 *
		 * @param Event $event
		 * @param Dispatcher $dispatcher
		 * @param \Exception $exception
		 */
		public function beforeException(Event $event, Dispatcher $dispatcher, \Exception $exception){
			if ($event->getType() == 'beforeException') {
				if($exception->getCode() == \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND){
					$di = FactoryDefault::getDefault();
					$router = $di->get('router');
					$slug = $router->getControllerName();
					$dispatcher->forward(array(
							'controller' => 'Page',
							'action' => 'load',
							'params' => ['slug'=>$slug]
					));
					return false;
				}
			}
		}

		public function beforeHandleRequest(){
			$args = func_get_args();
			return false;
		}

		/**
		 * Intelligently add markup to plain text
		 * @param string $content
		 * @return unknown
		 */
		function format($content){
			// TODO: use some intermediate markup language or add a second column with plain/raw text in addition to formatted text.
			$content = str_replace("\n", '</p><p>', $content);
			return "<p>{$content}</p>";
		}

		function styles(){
			$di = FactoryDefault::getDefault();
			$stylesheets = $di->get('site')->stylesheets();
			$string = '';
			foreach ($stylesheets as $stylesheet){
				$string .= $di->get('tag')->stylesheetLink($stylesheet->filepath());
			}
			return $string;
		}

		function scripts($location = 'head'){
			$di = FactoryDefault::getDefault();
			$scripts;
			switch ($location){
				default:
				case 'head':
					$scripts = $di->get('site')->headScripts();
					break;
				case 'footer':
					$scripts = $di->get('site')->footerScripts();
					break;
			}
			$string = '';
			foreach ($scripts as $script){
				$string .= $di->get('tag')->javascriptInclude($script->filepath(), 0!==stripos($script->filepath(), 'http'));
			}
			return $string;
		}
	}
}