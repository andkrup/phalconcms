<?php
namespace supervillainhq\spectre\cms{
	trait FileResourcing{
		protected $filepath;

		function filepath($filepath = null){
			if(is_null($filepath)){
				return $this->filepath;
			}
			$this->filepath = $filepath;
		}
	}
}