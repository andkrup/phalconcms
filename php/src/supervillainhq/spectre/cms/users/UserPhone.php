<?php
namespace supervillainhq\spectre\cms\users{
	use supervillainhq\core\contacts\PhoneNumber;
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\db\DataReader;

	class UserPhone extends PhoneNumber{
		use DataAware;

		protected $user;
		protected $context;
		protected $primary;

		function user(User $user = null){
			if(!is_null($user)){
				$this->user = $user;
			}
			return $this->user;
		}
		
		function context($context){
			if(!is_null($context)){
				$this->context = $context;
			}
			return $this->context;
		}

		function primary( $bool = null){
			if(!is_null($bool)){
				$this->primary = $bool;
			}
			return $this->primary;
		}

		static function inflate(DataReader $reader){
			$instance = new UserPhone();
			$instance->id = $reader->id;
			$instance->number = $reader->number;
			$instance->context = $reader->context;
			$instance->primary = $reader->primary;
			return $instance;
		}
	}
}