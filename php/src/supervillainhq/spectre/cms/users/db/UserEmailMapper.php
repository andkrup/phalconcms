<?php
namespace supervillainhq\spectre\cms\users\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\cms\users\UserEmail;

	class UserEmailMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof UserEmail){
					$this->addParameter('id', $data->id());
					$this->addParameter('primary', $data->primary());
					$this->addParameter('email', $data->emailAddress());
// 					$this->addParameter('user_id', $data->user()->id());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('email_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$sql = "select
						ue.id as email_id, ue.email as email_email, ue.primary as email_primary, ue.user_id as email_user_id
					from cms_UserEmails ue
					where ue.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			$row = $query->fetch();
			if($row){
				$emailMapper = $this->getDI()->getObjectmapper('useremail', (array) $row);
				$email = $emailMapper->inflate();
				return $email;
			}
			return null;
		}

		function get(){
			return $this->find();
		}

		function exists(){
			$sql = "select 1 as existing
					from cms_UserEmails ue
					where ue.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			return $query->fetchValue('existing', SqlQuery::TYPE_BOOLEAN);
		}

		function all(){
			$sql = "select
						ue.id as email_id, ue.email as email_email, ue.primary as email_primary, ue.user_id as email_user_id
						, u.id as user_id, u.username as user_username, u.activated as user_activated, u.last_login as user_last_login
					from cms_UserEmails ue
					left join cms_Users u on u.id = ue.user_id;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$emails = [];
			foreach ($rows as $row){
				$emailMapper = $this->getDI()->getObjectmapper('useremail', (array) $row);
				if($email = $emailMapper->inflate()){
					$userMapper = $this->getDI()->getObjectmapper('user', (array) $row);
					if($user = $userMapper->inflate()){
						$email->user($user);
}
					array_push($emails, $email);
}
			}
			return $emails;
		}

		function reset(array $data = null){
			$this->resetParameters($data);
		}

		function inflate(){
			return UserEmail::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
					case 'user':
					case 'user_id':
						return intval($this->getParameter($name));
					case 'primary':
						return intval($this->getParameter($name)) > 0;
					case 'email':
					case 'emailaddress':
					case 'email_address':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}