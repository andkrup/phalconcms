<?php
namespace supervillainhq\spectre\cms\users\db{

	use supervillainhq\spectre\cms\users\UserPhone;
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\cms\Section;

	class UserPhoneMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof UserPhone){
					$this->addParameter('id', $data->id());
					$this->addParameter('number', $data->number());
					$this->addParameter('primary', $data->primary());
					$this->addParameter('user', $data->user()->id());
					$this->addParameter('country', $data->country()->id());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('phone_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$sql = "select
						up.id as phone_id, up.phonenumber as phone_phonenumber, up.primary as phone_primary
						, c.id as country_id, c.iso as country_iso, c.iso3 as country_iso3, c.nicename as country_name, c.local_name as country_local_name, c.phonecode as country_callcode
						, u.id as user_id, u.username as user_username, u.activated as user_activated, u.last_login as user_last_login
					from cms_UserPhonenumbers up
					left join Countries c on c.id = up.country_id
					left join cms_Users u on u.id = up.user_id
					where up.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			$row = $query->fetch();
			if($row){
				$phoneMapper = $this->getDI()->getObjectmapper('userphone', (array) $row);
				if($phone = $phoneMapper->inflate()){
					$countryMapper = $this->getDI()->getObjectmapper('country', (array) $row);
					if($country = $countryMapper->inflate()){
						$phone->country($country);
					}
					$userMapper = $this->getDI()->getObjectmapper('user', (array) $row);
					if($user = $userMapper->inflate()){
						$phone->user($user);
					}

					return $phone;
				}
			}
			return null;
		}

		function get(){
			return $this->find();
		}

		function exists(){
			$sql = "select 1 as existing
					from cms_UserPhonenumbers s
					where s.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			return $query->fetchValue('existing', SqlQuery::TYPE_BOOLEAN);
		}

		function all(){
			$sql = "select
						up.id as phone_id, up.phonenumber as phone_phonenumber, up.primary as phone_primary
						, c.id as country_id, c.iso as country_iso, c.iso3 as country_iso3, c.nicename as country_name, c.local_name as country_local_name, c.phonecode as country_callcode
						, u.id as user_id, u.username as user_username, u.activated as user_activated, u.last_login as user_last_login
					from cms_UserPhonenumbers up
					left join Countries c on c.id = up.country_id
					left join cms_Users u on u.id = up.user_id;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$phones = [];
			foreach ($rows as $row){
				$phoneMapper = $this->getDI()->getObjectmapper('userphone', (array) $row);
				if($phone = $phoneMapper->inflate()){
					$countryMapper = $this->getDI()->getObjectmapper('country', (array) $row);
					if($country = $countryMapper->inflate()){
						$phone->country($country);
					}
					$userMapper = $this->getDI()->getObjectmapper('user', (array) $row);
					if($user = $userMapper->inflate()){
						$phone->user($user);
					}
					array_push($phones, $phone);
				}
			}
			return $phones;
		}

		function reset(array $data = null){
			$this->resetParameters($data);
		}

		function inflate(){
			return UserPhone::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
 					case 'user':
 					case 'country':
						return intval($this->getParameter($name));
					case 'number':
						return stripslashes(trim($this->getParameter($name)));
					case 'primary':
						return intval($this->getParameter($name)) == 1;
				}
			}
			return null;
		}
	}
}