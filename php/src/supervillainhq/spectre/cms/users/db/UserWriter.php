<?php
namespace supervillainhq\spectre\cms\users\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\DependencyInjecting;
	use supervillainhq\spectre\cms\users\User;

	class UserWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof User){
					$this->addParameter('id', $data->id());
					$this->addParameter('username', $data->name());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('user_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into cms_Users
					(username, created, updated)
					values (:username, now(), now());";
			$parameters = [
					'username' => $this->username
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return intval($query->lastInsertId());
		}

		function update(){
			$sql = "update cms_Users set
					username = :username,
					updated = now()
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'username' => $this->username
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}

		function delete(){
			$sql = "delete from cms_Users
					where id = :id;";
			$query = SqlQuery::create($sql);
			return $query->execute(['id' => $this->id]);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'name':
					case 'username':
						return stripslashes(trim($this->getParameter($name)));
				}
			}
			return null;
		}
	}
}