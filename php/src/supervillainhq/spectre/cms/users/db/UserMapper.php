<?php
namespace supervillainhq\spectre\cms\users\db{
//	use supervillainhq\core\mail\Email;
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\cms\users\User;
	use supervillainhq\spectre\contacts\SimpleAddressBook;

	class UserMapper extends Mapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof User){
					$this->addParameter('id', $data->id());
					$this->addParameter('username', $data->name());
					$this->addParameter('activated', $data->activated());
					$this->addParameter('last_login', $data->lastLogin()->format('Y-m-d H:i:s'));
					$this->addParameter('created', $data->created()->format('Y-m-d H:i:s'));
					$this->addParameter('updated', $data->updated()->format('Y-m-d H:i:s'));
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('user_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$sql = "select
						u.id as user_id, u.username as user_username, u.activated as user_activated, u.last_login as user_last_login
						, ue.id as email_id, ue.email as email_emailaddress, ue.primary as email_primary
						, p.id as privilege_id, p.name as privilege_name, p.description as privilege_description
					from cms_Users u
					left join cms_UserEmails ue on ue.user_id = u.id and ue.primary = 1
					left join cms_UserPrivileges up on up.user_id = u.id
					left join cms_Privileges p on p.id = up.privilege_id
					where u.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			$rows = $query->fetchAll();
			$user = null;
			foreach ($rows as $row){
				if(is_null($user)){
					$userMapper = $this->getDI()->getObjectmapper('user', (array) $row);
					$user = $userMapper->inflate();
					$user->info(new SimpleAddressBook());
				}
				$mapper = $this->getDI()->getObjectmapper('privilege', (array) $row);
				$privilege = $mapper->inflate();
				$user->addPrivilege($privilege->name(), $privilege);
			}
			return $user;
		}

		function get(){
			return $this->find();
		}

		function exists(){
			$sql = "select 1 as existing
					from cms_Users u
					where u.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			return $query->fetchValue('existing', SqlQuery::TYPE_BOOLEAN);
		}

		function all(){
			$sql = "select
						u.id as user_id, u.username as user_username, u.activated as user_activated, u.last_login as user_last_login
						, ue.id as email_id, ue.email as email_emailaddress, ue.primary as email_primary
					from cms_Users u
					left join cms_UserEmails ue on ue.user_id = u.id and ue.primary = 1;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$users = [];
			foreach ($rows as $row){
				$userMapper = $this->getDI()->getObjectmapper('user', (array) $row);
				if($user = $userMapper->inflate()){
					array_push($users, $user);
				}
			}
			return $users;
		}

		function reset(array $data = null){
			$this->resetParameters($data);
		}

		function inflate(){
			return User::inflate($this);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
						return intval($this->getParameter($name));
					case 'username':
						return stripslashes(trim($this->getParameter($name)));
					case 'created':
					case 'updated':
					case 'last_login':
						return \DateTime::createFromFormat('Y-m-d H:i:s', $this->getParameter($name));
					case 'active':
					case 'activated':
						return intval($this->getParameter($name)) > 0;
// 					case 'email':
// 						return new Email($this->getParameter($name));
				}
			}
			return null;
		}
	}
}