<?php
namespace supervillainhq\spectre\cms\users\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\core\db\ParameterContainer;
	use supervillainhq\core\db\Reading;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\DependencyInjecting;
	use supervillainhq\spectre\cms\users\UserEmail;

	class UserEmailWriter implements DataWriter{
		use DependencyInjecting, ParameterContainer, Reading;

		function __construct($data){
			if(!is_null($data)){
				if($data instanceof UserEmail){
					$this->addParameter('id', $data->id());
					$this->addParameter('email', $data->emailAddress());
					$this->addParameter('user', $data->user()->id());
				}
				elseif (is_array($data)){
					$this->resetParameters();
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('email_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sql = "insert into cms_UserEmails
					(email, user_id, `primary`)
					values (:email, :user, :primary);";
			$parameters = [
					'email' => $this->email,
					'user' => $this->user_id,
					'primary' => $this->primary
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $query->lastInsertId();
		}

		function update(){
			$sql = "update cms_UserEmails set
					email = :email,
					user_id = :user
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'email' => $this->email,
					'user' => $this->user
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}

		function delete(){
			$sql = "delete from cms_UserEmails
					where id = :id;";
			$query = SqlQuery::create($sql);
			return $query->execute(['id' => $this->id]);
		}

		function __get($name){
			if($this->hasParameterAtKey($name)){
				switch($name){
					case 'id':
					case 'user':
					case 'user_id':
						return intval($this->getParameter($name));
					case 'email':
						return stripslashes(trim($this->getParameter($name)));
					case 'primary':
						return intval($this->getParameter($name)) == 1;
				}
			}
			return null;
		}
	}
}