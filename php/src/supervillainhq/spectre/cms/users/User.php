<?php
namespace supervillainhq\spectre\cms\users{
	use supervillainhq\spectre\auth\AuthUser;
	use supervillainhq\spectre\contacts\Contact;
	use supervillainhq\spectre\contacts\ContactInfo;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\auth\Privilege;

	/**
	 * Specialized user that can be assigned privileges on a website
	 * @author ak
	 *
	 */
	class User extends AuthUser implements Contact{

		protected $contactInfo;
		protected $privileges;
		protected $lastLogin;

		function info(ContactInfo $info = null){
			if(is_null($info)){
				return $this->contactInfo;
			}
			$this->contactInfo = $info;
		}

		function resetPrivileges(array $privileges = []){
			$this->privileges = $privileges;
		}
		function addPrivilege($key, Privilege $privilege){
			$this->privileges[$key] = $privilege;
		}
		function removePrivilege(Privilege $privilege){
			$c = count($this->privileges);
			for($i = 0; $i < $c; $i++){
				if($this->privileges[$i] == $privilege){
					array_splice($this->privileges, $i, 0);
				}
			}
		}
		function removePrivilegeAt($key){
			unset($this->privileges[$key]);
		}
		function hasPrivilege(Privilege $privilege){
			foreach ($this->privileges as $key => $priv){
				if($privilege->equals($priv)){
					return true;
				}
			}
			return false;
		}
		function hasPrivilegeAtKey($key){
			return array_key_exists($key, $this->privileges);
		}
		function getPrivilege($key){
			return $this->privileges[$key];
		}
		function privileges(){
			return $this->privileges;
		}

		function lastLogin(\DateTime $date = null){
			if(is_null($date)){
				return $this->lastLogin;
			}
			$this->lastLogin = $date;
		}

		function __construct(){
			parent::__construct();
			$this->resetPrivileges();
		}


		static function inflate(DataReader $reader){
			if($reader->emptyParameters('user')){
				return null;
			}
			$instance = new User();
			$instance->id = $reader->id;
			$instance->userName = $reader->username;
			$instance->active = $reader->activated;
			$instance->lastLogin = $reader->last_login;
			$instance->created = $reader->created;
			$instance->updated = $reader->updated;
// 			$instance->primaryEmail = $reader->email;
// 			$instance->contactInfo = $reader->contactinfo;
			return $instance;
		}
	}
}