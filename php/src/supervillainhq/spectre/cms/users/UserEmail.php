<?php
namespace supervillainhq\spectre\cms\users{
	use supervillainhq\core\mail\Email;
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\db\DataReader;

	class UserEmail extends Email{
		use DataAware;

		protected $user;
		protected $primary;

		function __construct($emailAddress = ''){
			parent::__construct($emailAddress, false);
		}

		function user(User $user = null){
			if(is_null($user)){
				return $this->user;
			}
			$this->user = $user;
		}
		function primary( $bool = null){
			if(is_null($bool)){
				return $this->primary;
			}
			$this->primary = $bool;
		}

		static function inflate(DataReader $reader){
			$instance = new UserEmail();
			$instance->emailAddress = $reader->email;
			$instance->primary = $reader->primary;
			return $instance;
		}
	}
}