<?php
namespace supervillainhq\spectre\cms\assets{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\spectre\db\DataReader;

	class Mimetype{
		use DataAware;

		protected $name;
		protected $description;
		protected $extension;
		protected $editor;

		function name($name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}
		function extension($extension = null){
			if(is_null($extension)){
				return $this->extension;
			}
			$this->extension = $extension;
		}
		function editor(AssetEditor $editor = null){
			if(is_null($editor)){
				return $this->editor;
			}
			$this->editor = $editor;
		}

		static function create(DataReader $reader){
			$instance = new Mimetype();
			$instance->id = $reader->id;
			$instance->name = $reader->name;
			$instance->extension = $reader->extension;
			return $instance;
		}

		static function getByExtension($extension){
			$imagetypes = self::imageMimetypes();
			foreach ($imagetypes as $ext => $mimetype){
				if(stripos($extension, $ext) !== false){
					$instance = new Mimetype();
					$instance->name($mimetype);
					$instance->extension($ext);
					return $instance;
				}
			}
		}
		static function getByType($type){
			$imagetypes = self::imageMimetypes();
			foreach ($imagetypes as $extension => $mimetype){
				if(stripos($type, $mimetype) !== false){
					$instance = new Mimetype();
					$instance->name($mimetype);
					$instance->extension($extension);
					return $instance;
				}
			}
		}

		static function imageMimetypes(){
			return [
					'jpg' => 'image/jpeg',
					'jpeg' => 'image/jpeg',
					'png' => 'image/png',
					'gif' => 'image/gif',
			];
		}
	}
}