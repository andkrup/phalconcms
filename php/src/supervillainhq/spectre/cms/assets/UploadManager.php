<?php
namespace supervillainhq\spectre\cms\assets{
	use Phalcon\Http\Request\File as RequestFile;
	use supervillainhq\spectre\cms\commands\UploadAssetCommand;
	use supervillainhq\spectre\Erroneous;

	class UploadManager{
		use Erroneous;

		private $config;

		function __construct($config){
			$this->config = $config;
		}

		function handleUploads(array $files){
			$this->resetErrors();
			$assets = [];
			foreach ($files as $file){
				if($file instanceof RequestFile){
					$asset = null;
					$command = new UploadAssetCommand($this->config, $file);
					try{
						$asset = $command->execute();
					}
					catch(\Exception $exception){
						$this->addError($exception->getMessage());
					}
					if(!is_null($asset)){
						array_push($assets, $asset);
					}
				}
			}
			return $assets;
		}
	}
}