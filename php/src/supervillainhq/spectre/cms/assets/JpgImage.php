<?php
namespace supervillainhq\spectre\cms\assets{
	class JpgImage extends Mimetype{
		function __construct($extension){
			$this->name('image/jpg');
			$this->extension($extension);
		}
	}
}