<?php
namespace supervillainhq\spectre\cms\assets{
	use supervillainhq\spectre\db\SqlQuery;
	
	class Assets{
		static function createAsset(){}
		static function createMimetype($name, $extension, AssetEditor $editor){
			$mime = new Mimetype();
			$mime->name($name);
			$mime->extension($extension);
		}

		static function routes(FileAsset $asset){
			$sql = "select
						ra.route as asset_route
					from cms_RequestAssets ra
					left join cms_Assets a on a.id = ra.asset_id
					where a.id= :asset";
			$query = SqlQuery::create($sql);
			$query->query(['asset' => $asset->id()]);
			$rows = $query->fetchAll();
			$routes = [];
			foreach ($rows as $row){
				$row = (object) $row;
				array_push($routes, $row->asset_route);
			}
			return $routes;
		}
	}
}