<?php
namespace supervillainhq\spectre\cms\assets{
	use supervillainhq\spectre\cms\Editor;

	class FileUploader implements Editor{
		private $uploadManager;

		function uploadManager(UploadManager $uploadManager = null){
			if(is_null($uploadManager)){
				return $this->uploadManager;
			}
			$this->uploadManager = $uploadManager;
		}
	}
}