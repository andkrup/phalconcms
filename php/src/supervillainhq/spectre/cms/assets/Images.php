<?php
namespace supervillainhq\spectre\cms\assets{
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\db\AssetMapper;
	use supervillainhq\spectre\db\MimetypeMapper;
	use supervillainhq\spectre\cms\assets\Mimetype;

	class Images{
		static function getAssets($uri){
			$sql = "select
						a.id as asset_id, a.name as asset_name, a.src as asset_filepath
						, et.name as editor_name
						, m.extension, m.mimetype
					from cms_RequestAssets ra
					left join cms_Assets a on a.id = ra.asset_id
					left join cms_Mimetypes m on m.id = a.mimetype_id
					left join cms_EditorTypes et on et.id = m.editortype_id
					where ra.route = :request";
			$query = SqlQuery::create($sql);
			$query->query(['request' => $uri]);
			$rows = $query->fetchAll();
			$assets = [];
			foreach ($rows as $row){
				$asset = FileAsset::create(new AssetMapper((array) $row));
				$asset->mimetype(Mimetype::create(new MimetypeMapper((array) $row)));
				array_push($assets, $asset);
			}
			return $assets;
		}
		static function imageAssets(){
			$sql = "select
						a.id as asset_id, a.name as asset_name, a.src as asset_filepath
						, et.name as editor_name
						, m.extension, m.mimetype
					from cms_Assets a
					left join cms_Mimetypes m on m.id = a.mimetype_id
					left join cms_EditorTypes et on et.id = m.editortype_id
					where et.name = 'image';";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$assets = [];
			foreach ($rows as $row){
				$asset = FileAsset::create(new AssetMapper((array) $row));
				$asset->mimetype(Mimetype::create(new MimetypeMapper((array) $row)));
				array_push($assets, $asset);
			}
			return $assets;
		}
	}
}