<?php
namespace supervillainhq\spectre\cms\assets{
	use supervillainhq\spectre\cms\Editor;

	interface AssetEditor extends Editor{
		function loadAsset(FileAsset $asset);
		function update();
		function delete();
	}
}