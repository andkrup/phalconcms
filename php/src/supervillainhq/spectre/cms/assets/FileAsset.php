<?php
namespace supervillainhq\spectre\cms\assets{
	use supervillainhq\spectre\cms\FileResourcing;
	use supervillainhq\spectre\auth\AuthUser;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\core\db\DataAware;

	class FileAsset{
		use FileResourcing, DataAware;

		protected $user;
		protected $name;
		protected $mimetype;

		function __construct($filepath = null){
			$this->filepath = $filepath;
		}

		function owner(AuthUser $user = null){
			if(is_null($user)){
				return $this->user;
			}
			$this->user = $user;
		}
		function name($name = null){
			if(is_null($name)){
				return $this->name;
			}
			$this->name = $name;
		}
		function mimetype(Mimetype $mimetype = null){
			if(is_null($mimetype)){
				return $this->mimetype;
			}
			$this->mimetype = $mimetype;
		}
		function editor(AssetEditor $editor = null){
			if(is_null($editor)){
				if($this->mimetype instanceof Mimetype){
					return $this->mimetype->editor();
				}
				return null;
			}
			if($this->mimetype instanceof Mimetype){
				$this->mimetype->editor($editor);
			}
		}

		function url($path = '/files'){
			$name = $this->name();
			return "{$path}/{$name}";
		}

		static function inflate(DataReader $reader){
			$instance = new FileAsset();
			$instance->id = $reader->id;
			$instance->name = $reader->name;
			$instance->mimetype = $reader->mimetype;
			$instance->filepath = $reader->filepath;
			return $instance;
		}
	}
}