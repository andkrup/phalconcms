<?php
namespace supervillainhq\spectre\cms\controllers{

	trait PageControlling{

		/**
		 * Fetch and display a dynamic page by its slug
		 *
		 * @param string $slug
		 */
		function bySlugAction($slug){
			$page = $this->getDI()->getObjectmapper('page', ['slug' => $slug])->find();
			$template = $page->template();
			$viewpath = $template->templatePath();
			$this->view->pick($viewpath);
			$this->view->setVar('page', $page);
		}
	}
}