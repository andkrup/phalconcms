<?php
namespace supervillainhq\spectre\cms\controllers{
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\Mvc\View;
	use supervillainhq\spectre\json\JsonView;
	use supervillainhq\spectre\cms\Stylesheet;
	use supervillainhq\spectre\cms\Javascript;

	class CmsController extends \ControllerBase{
		use PageControlling;

		protected $stylesheets;
		protected $headJavascripts;
		protected $footerJavascripts;

		function initialize(){
			$this->view->styles = $this->stylesheets;
			$this->view->headScripts = $this->headJavascripts;
			$this->view->footerScripts = $this->footerJavascripts;
		}

		function resetStylesheets(array $styleheets = []){
			$this->stylesheets = $styleheets;
		}
		function addStylesheet($key, Stylesheet $styleheet){
			$this->stylesheets[$key] = $styleheet;
		}
		function removeStylesheet(Stylesheet $styleheet){
			$c = count($this->stylesheets);
			for($i = 0; $i < $c; $i++){
				if($this->stylesheets[$i] == $styleheet){
					array_splice($this->stylesheets, $i, 0);
				}
			}
		}
		function removeStylesheetAt($key){
			unset($this->stylesheets[$key]);
		}
		function hasStylesheet(Stylesheet $styleheet){
			return in_array($this->stylesheets, $styleheet);
		}
		function hasStylesheetAtKey($key){
			return array_key_exists($key, $this->stylesheets);
		}
		function getStylesheet($key){
			return $this->stylesheets[$key];
		}
		function stylesheets(){
			return $this->stylesheets;
		}

		protected function requireStylesheets(array $stylesheetTokens){
			$stylesheets = (object) $this->config->stylesheets;
			foreach ($stylesheetTokens as $token){
				if($this->site->hasStylesheetAtKey($token)){
					continue;
				}
				if(property_exists($stylesheets, $token)){
					$stylesheetInfo = $stylesheets->{$token};
					if(property_exists($stylesheetInfo, 'depends')){
						$dependencies = (array) $stylesheetInfo->depends;
						$this->requireStylesheets($dependencies);
					}
					$source = $stylesheetInfo->source;
					$this->site->addStylesheet($token, new Stylesheet($source));
				}
			}
		}


		function resetHeadJavascripts(array $javascripts = []){
			$this->headJavascripts = $javascripts;
		}
		function addHeadJavascript($key, Javascript $javascript){
			$this->headJavascripts[$key] = $javascript;
		}
		function removeHeadJavascript(Javascript $javascript){
			$c = count($this->headJavascripts);
			for($i = 0; $i < $c; $i++){
				if($this->headJavascripts[$i] == $javascript){
					array_splice($this->headJavascripts, $i, 0);
				}
			}
		}
		function removeHeadJavascriptAt($key){
			unset($this->headJavascripts[$key]);
		}
		function hasHeadJavascript(Javascript $javascript){
			return in_array($this->headJavascripts, $javascript);
		}
		function hasHeadJavascriptAtKey($key){
			return array_key_exists($key, $this->headJavascripts);
		}
		function getHeadJavascript($key){
			return $this->headJavascripts[$key];
		}
		function headJavascripts(){
			return $this->headJavascripts;
		}

		protected function requireHeadScripts(array $scriptTokens){
			$scripts = (object) $this->config->scripts;
			foreach ($scriptTokens as $token){
				if($this->site->hasHeadScriptAtKey($token)){
					continue;
				}
				if(property_exists($scripts, $token)){
					$scriptInfo = $scripts->{$token};
					if(property_exists($scriptInfo, 'depends')){
						$dependencies = (array) $scriptInfo->depends;
						$this->requireHeadScripts($dependencies);
					}
					$source = $scriptInfo->source;
					$this->site->addHeadScript($token, new Javascript($source));
				}
			}
		}

		function resetFooterJavascripts(array $javascripts = []){
			$this->footerJavascripts = $javascripts;
		}
		function addFooterJavascript($key, Javascript $javascript){
			$this->footerJavascripts[$key] = $javascript;
		}
		function removeFooterJavascript(Javascript $javascript){
			$c = count($this->footerJavascripts);
			for($i = 0; $i < $c; $i++){
				if($this->footerJavascripts[$i] == $javascript){
					array_splice($this->footerJavascripts, $i, 0);
				}
			}
		}
		function removeFooterJavascriptAt($key){
			unset($this->footerJavascripts[$key]);
		}
		function hasFooterJavascript(Javascript $javascript){
			return in_array($this->footerJavascripts, $javascript);
		}
		function hasFooterJavascriptAtKey($key){
			return array_key_exists($key, $this->footerJavascripts);
		}
		function getFooterJavascript($key){
			return $this->footerJavascripts[$key];
		}
		function footerJavascripts(){
			return $this->footerJavascripts;
		}

		/**
		 * Changes to a json view upon ajax detection
		 * @param Dispatcher $dispatcher
		 */
		function afterExecuteRoute(Dispatcher $dispatcher) {
			if ($this->request->isAjax()) {
				$this->config->is_ajax = true;
				$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
				$this->response->setContentType('application/json', 'UTF-8');

				/* hook to afterRender event */
				$eventManager = $this->view->getEventsManager();
				if (is_null($eventManager)){
					$eventManager = new \Phalcon\Events\Manager();
					$this->view->setEventsManager($eventManager);
				}
				$jsonview = new JsonView();
				$jsonview->setDI($this->getDI());
				$eventManager->attach("view:afterRender", $jsonview);
		    }
		}
	}
}