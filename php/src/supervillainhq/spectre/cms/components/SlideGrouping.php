<?php
namespace supervillainhq\spectre\cms\components{

	trait SlideGrouping{
		protected $slides;

		function resetSlides(array $slides = []){
			$this->slides = $slides;
		}
		function addSlide(ContentSlide $slide){
			array_push($this->slides, $slide);
		}
		function removeSlide(ContentSlide $slide){
			$c = count($this->slides);
			for($i = 0; $i < $c; $i++){
				if($this->slides[$i] == $slide){
					array_splice($this->slides, $i, 0);
				}
			}
		}
		function hasSlide(ContentSlide $slide){
			return in_array($this->slides, $slide);
		}
		function getSlide($index){
			return $this->slides[$index];
		}
		function slides(){
			return $this->slides;
		}

	}
}