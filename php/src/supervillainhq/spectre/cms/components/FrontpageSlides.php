<?php
namespace supervillainhq\spectre\cms\components{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\core\locale\Localizing;

	class FrontpageSlides implements SlideGroup{
		use DataAware, Localizing, SlideGrouping;

		protected $name;
		protected $from;
		protected $to;

		function name($name = null){
			if(!is_null($name)){
				$this->name = $name;
			}
			return $this->name;
		}

		function from(\DateTime $date = null){
			if(!is_null($date)){
				$this->from = $date;
			}
			return $this->from;
		}

		function to(\DateTime $date = null){
			if(!is_null($date)){
				$this->to = $date;
			}
			return $this->to;
		}
	}
}