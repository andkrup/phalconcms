<?php
namespace supervillainhq\spectre\cms\components\db{
	use supervillainhq\core\db\DataWriter;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\components\ContentSlide;
	use supervillainhq\spectre\cms\db\SectionWriter;

	class SliderWriter extends SectionWriter implements DataWriter{

		function __construct($data){
			parent::__construct($data);
			if(!is_null($data)){
				if($data instanceof ContentSlide){
					$this->addParameter('id', $data->id());
					$this->addParameter('template', $data->template()->id());
				}
				elseif (is_array($data)){
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('slider_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}
		function create(){
			$sectionId = parent::create();
			$sql = "insert into cmp_SliderSections
					(id, template)
					values (:id, :template);";
			$parameters = [
					'id' => $this->id,
					'template' => $this->template
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
			return $sectionId;
		}
		function update(){
			parent::update();
			$sql = "update cmp_SliderSections set
					template = :template
					where id = :id;";
			$parameters = [
					'id' => $this->id,
					'template' => $this->template
			];
			$query = SqlQuery::create($sql);
			$result = $query->execute($parameters);
		}

		function delete(){
			$deleted = parent::delete();
			if($deleted){
				$sql = "delete from cmp_SliderSections
						where id = :id;";
				$query = SqlQuery::create($sql);
				return $query->execute(['id' => $this->id]);
			}
			return $deleted;
		}

		function __get($name){
			$val = parent::__get($name);
			if(is_null($val)){
				if($this->hasParameterAtKey($name)){
					switch($name){
						case 'template':
							return stripslashes(trim($this->getParameter($name)));
					}
				}
			}
			return $val;
		}
	}
}