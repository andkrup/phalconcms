<?php
namespace supervillainhq\spectre\cms\components\db{
	use supervillainhq\spectre\db\DataMapper;
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\db\Mapper;
	use supervillainhq\spectre\db\SqlQuery;
	use supervillainhq\spectre\cms\components\ContentSlide;
	use supervillainhq\spectre\cms\db\SectionMapper;

	class SliderMapper extends SectionMapper implements DataMapper, DataReader{

		function __construct($data = null, $lazyLoad = true){
			parent::__construct($data, $lazyLoad);
			if(!is_null($data)){
				if($data instanceof ContentSlide){
// 					$this->addParameter('id', $data->id());
					$this->addParameter('template', $data->template());
				}
				elseif (is_array($data)){
					$keys = array_keys($data);
					foreach ($keys as $key){
						$k = str_ireplace('slide_', '', $key);
						$this->addParameter($k, $data[$key]);
					}
				}
			}
		}

		function find(){
			$sql = "select
						ss.id as slide_id, ss.template as slide_template
						, s.id as section_id, s.title as section_title, s.content_html as section_content_html
					from cmp_SliderSections ss
					left join cms_Sections s on s.id = ss.id
					where s.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			$row = $query->fetch();
			if(isset($row)){
				$mapper = $this->getDI()->getObjectmapper('slide', (array) $row);
				return $mapper->inflate();
			}
			return null;
		}

		function get(){
			return $this->find();
		}

		function exists(){
			$sql = "select 1 as existing
					from cmp_SliderSections ss
					where ss.id = :id;";
			$query = SqlQuery::create($sql);
			$query->query(['id' => $this->getParameter('id')]);
			return $query->fetchValue('existing', SqlQuery::TYPE_BOOLEAN);
		}

		function all(){
			$sql = "select
						ss.id as slide_id, ss.template as slide_template
						, s.id as section_id, s.title as section_title, s.content_html as section_content_html
					from cmp_SliderSections ss
					left join cms_Sections s on s.id = ss.id;";
			$query = SqlQuery::create($sql);
			$query->query();
			$rows = $query->fetchAll();
			$slides = [];
			foreach ($rows as $row){
				$mapper = $this->getDI()->getObjectmapper('slide', (array) $row);
				$slide = $mapper->inflate();
				array_push($slides, $slide);
			}
			return $slides;
		}

		function reset(array $data = null){
			parent::reset($data);
		}

		function inflate(){
			return ContentSlide::inflate($this);
		}

		function __get($name){
			$val = parent::__get($name);
	    	if(is_null($val)){
	    		switch($name){
	    			case 'line_dia':
	    				return floatval($this->getParameter($name));
	    		}
	    	}
	    	return $val;
		}
	}
}