<?php
namespace supervillainhq\spectre\cms\components{
	use Phalcon\Di\InjectionAwareInterface;
	use Phalcon\DiInterface;
	use Phalcon\Config;
	use supervillainhq\spectre\DependencyInjecting;

	class FrontpageSlider implements InjectionAwareInterface{
		use DependencyInjecting;

		private $config;

		function __construct(DiInterface $dependencyInjector, Config $config){
			$this->setDI($dependencyInjector);
			$this->config = $config;
		}

		function slides(){
			$mapper = $this->di->getObjectmapper('slide');
			return $mapper->all();
		}
	}
}