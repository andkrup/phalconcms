<?php
namespace supervillainhq\spectre\cms\components{
	use supervillainhq\spectre\db\DataReader;
	use supervillainhq\spectre\posting\Author;
	use supervillainhq\spectre\cms\Section;

	class ContentSlide extends Section implements \JsonSerializable{

		protected $author;
		protected $template;
		protected $group;
		protected $priority;
		protected $start;
		protected $end;

		function author(Author $author = null){
			if(is_null($author)){
				return $this->author;
			}
			$this->author = $author;
		}
		function template( $template = null){
			if(is_null($template)){
				return $this->template;
			}
			$this->template = $template;
		}
		function group(SlideGroup $group = null){
			if(is_null($group)){
				return $this->group;
			}
			$this->group = $group;
		}
		function priority( $priority = null){
			if(is_null($priority)){
				return $this->priority;
			}
			$this->priority = $priority;
		}
		function start(\DateTime $date = null){
			if(is_null($date)){
				return $this->start;
			}
			$this->start = $date;
		}
		function end(\DateTime $date = null){
			if(is_null($date)){
				return $this->end;
			}
			$this->end = $date;
		}


		static function inflate(DataReader $reader, $instance = null){
			$instance = Section::inflate($reader, new ContentSlide());
			$instance->author = $reader->author;
			$instance->group = $reader->group;
			$instance->priority = $reader->priority;
			$instance->start = $reader->start;
			$instance->end = $reader->end;
			return $instance;
		}

		function jsonSerialize () {
			$start = $this->start();
			$end = $this->end();
			$visibles = [
					'title' => $this->title(),
					'html' => $this->html(),
					'priority' => $this->priority(),
					'template' => $this->template(),
					'start' => $start ? $start->format(\DateTime::ATOM) : null,
					'end' => $end ? $end->format(\DateTime::ATOM) : null
			];
			return (object) $visibles;
		}
	}
}