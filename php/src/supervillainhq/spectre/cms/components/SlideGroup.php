<?php
namespace supervillainhq\spectre\cms\components{
	interface SlideGroup{
		function name($name = null);
		function from(\DateTime $date = null);
		function to(\DateTime $date = null);
		function resetSlides(array $slides = []);
		function addSlide(ContentSlide $slide);
		function removeSlide(ContentSlide $slide);
		function hasSlide(ContentSlide $slide);
		function getSlide($index);
		function slides();
	}
}