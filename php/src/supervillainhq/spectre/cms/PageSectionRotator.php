<?php
namespace supervillainhq\spectre\cms{
	/**
	 * Manages how sections are rotated on a specific page
	 *
	 * Should use cms_RotatingPageSections table as sub table for cms_PageSections
	 * @author ak
	 *
	 */
	class PageSectionRotator implements PageSectionManager{
		use PageSectionManaging;

		function __construct(){
			$this->resetSections();
		}
	}
}