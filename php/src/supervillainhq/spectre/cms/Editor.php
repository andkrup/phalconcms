<?php
namespace supervillainhq\spectre\cms{
	use Phalcon\Config;

	interface Editor{
		function name($name = null);
		function classpath($path = null);
		function config(Config $config = null);
		function dependencies();
		function init();
	}
}