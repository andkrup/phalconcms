<?php
namespace supervillainhq\spectre\cms{
	use supervillainhq\core\db\DataAware;
	use supervillainhq\core\locale\Localized;
	use supervillainhq\core\locale\Localizing;
	use supervillainhq\spectre\db\DataReader;

	class Section implements Localized{
		use DataAware, Localizing;

		protected $title;
		protected $created;
		protected $updated;
		private $html;
		private $raw;
		private $markup;

		function title($title = null){
			if(is_null($title)){
				return $this->title;
			}
			$this->title = $title;
		}

		function html($html = null){
			if(is_null($html)){
				return $this->html;
			}
			$this->html = $html;
		}
		function raw($raw = null){
			if(is_null($raw)){
				return $this->raw;
			}
			$this->raw = $raw;
		}
		function markup($markup = null){
			if(is_null($markup)){
				return $this->markup;
			}
			$this->markup = $markup;
		}
		function created(\DateTime $date = null){
			if(is_null($date)){
				return $this->created;
			}
			$this->created = $date;
		}
		function updated(\DateTime $date = null){
			if(is_null($date)){
				return $this->updated;
			}
			$this->updated = $date;
		}

		static function inflate(DataReader $reader, $instance = null){
			if(is_null($instance)){
				$instance = new Section();
			}
			$instance->id = $reader->id;
			$instance->title = $reader->title;
			$instance->raw = $reader->content_raw;
			$instance->html = $reader->content_html;
			$instance->markup = $reader->content_markup;
			$instance->created = $reader->created;
			$instance->updated = $reader->updated;
			return $instance;
		}
	}
}