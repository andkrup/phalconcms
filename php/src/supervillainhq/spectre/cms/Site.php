<?php
namespace supervillainhq\spectre\cms{
	class Site{
		private static $instance;
		private $metatags;
		private $stylesheets;
		private $headscripts;
		private $footerscripts;

		static function instance(){
			if(!isset(self::$instance)){
				self::$instance = new Site();
			}
			return self::$instance;
		}

		private function __construct(){
			$this->resetFooterscripts();
			$this->resetHeadScripts();
			$this->resetMetatags();
			$this->resetStylesheets();
		}

		function resetMetatags(array $metatags = []){
			$this->metatags = $metatags;
		}
		function addMetatag($key, $metatag){
			$this->metatags[$key] = $metatag;
		}
		function removeMetatag($metatag){
			$c = count($this->metatags);
			for($i = 0; $i < $c; $i++){
				if($this->metatags[$i] == $metatag){
					array_splice($this->metatags, $i, 0);
				}
			}
		}
		function removeMetatagAt($key){
			unset($this->metatags[$key]);
		}
		function hasMetatag($metatag){
			return in_array($this->metatags, $metatag);
		}
		function hasMetatagAtKey($key){
			return array_key_exists($key, $this->metatags);
		}
		function getMetatag($key){
			return $this->metatags[$key];
		}
		function metatags(){
			return $this->metatags;
		}

		function resetStylesheets(array $stylesheets = []){
			$this->stylesheets = $stylesheets;
		}
		function addStylesheet($key, Stylesheet $stylesheet){
			$this->stylesheets[$key] = $stylesheet;
		}
		function removeStylesheet(Stylesheet $stylesheet){
			$c = count($this->stylesheets);
			for($i = 0; $i < $c; $i++){
				if($this->stylesheets[$i] == $stylesheet){
					array_splice($this->stylesheets, $i, 0);
				}
			}
		}
		function removeStylesheetAt($key){
			unset($this->stylesheets[$key]);
		}
		function hasStylesheet(Stylesheet $stylesheet){
			return in_array($this->stylesheets, $stylesheet);
		}
		function hasStylesheetAtKey($key){
			return array_key_exists($key, $this->stylesheets);
		}
		function getStylesheet($key){
			return $this->stylesheets[$key];
		}
		function stylesheets(){
			return $this->stylesheets;
		}

		function resetHeadScripts(array $javascripts = []){
			$this->headscripts = $javascripts;
		}
		function addHeadScript($key, Javascript $javascript){
			$this->headscripts[$key] = $javascript;
		}
		function removeHeadScript(Javascript $javascript){
			$c = count($this->headscripts);
			for($i = 0; $i < $c; $i++){
				if($this->headscripts[$i] == $javascript){
					array_splice($this->headscripts, $i, 0);
				}
			}
		}
		function removeHeadScriptAt($key){
			unset($this->headscripts[$key]);
		}
		function hasHeadScript(Javascript $javascript){
			return in_array($this->headscripts, $javascript);
		}
		function hasHeadScriptAtKey($key){
			return array_key_exists($key, $this->headscripts);
		}
		function getHeadScript($key){
			return $this->headscripts[$key];
		}
		function headScripts(){
			return $this->headscripts;
		}

		function resetFooterScripts(array $javascripts = []){
			$this->footerscripts = $javascripts;
		}
		function addFooterScript($key, Javascript $javascript){
			$this->footerscripts[$key] = $javascript;
		}
		function removeFooterScript(Javascript $javascript){
			$c = count($this->footerscripts);
			for($i = 0; $i < $c; $i++){
				if($this->footerscripts[$i] == $javascript){
					array_splice($this->footerscripts, $i, 0);
				}
			}
		}
		function removeFooterScriptAt($key){
			unset($this->footerscripts[$key]);
		}
		function hasFooterScript(Javascript $javascript){
			return in_array($this->footerscripts, $javascript);
		}
		function hasFooterScriptAtKey($key){
			return array_key_exists($key, $this->footerscripts);
		}
		function getFooterScript($key){
			return $this->footerscripts[$key];
		}
		function footerScripts(){
			return $this->footerscripts;
		}
	}
}